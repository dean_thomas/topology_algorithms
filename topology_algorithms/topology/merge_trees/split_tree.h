#ifndef SPLIT_TREE_H
#define SPLIT_TREE_H

#include "merge_tree.h"

class SplitTree : public MergeTree
{
public:
	SplitTree();

	void createTree(HeightField3D *heightField);

	std::string ToDotString(HeightField3D *heightField) const
	{
		return MergeTree::ToDotString(heightField, true);
	}
};

#endif // SPLIT_TREE_H
