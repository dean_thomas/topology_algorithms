#ifndef VOL_1_H
#define VOL_1_H

#include "Vector3.h"
#include <string>
#include <fstream>
#include <cassert>
#include <vector>
#include <iostream>
#include <numeric>
#include <algorithm>

#ifdef _MSC_VER
#define __PRETTY_FUNCTION__ __FUNCSIG__
#endif

template <typename T>
struct Vol1Writer
{
	using value_type = T;
	using Array3 = Storage::Vector3<T>;
	using size_type = size_t;
	//const std::string EXT = "vol1";

	//	Todo: put this into a single struct
	std::string m_ensembleName;
	std::string m_fieldName;
	std::string m_configurationName;
	size_type m_cools;
	size_type m_slice;
	Array3 m_data;
public:
	///
	/// \brief Vol1FileWriter
	/// \param ensembleName
	/// \param fieldName
	/// \param configurationName
	/// \param cools
	/// \since	19-11-2015
	/// \author	Dean
	///
	Vol1Writer(const std::string& ensembleName,
			   const std::string& fieldName,
			   const std::string& configurationName,
			   const size_type& cools,
			   const size_type& slice,
			   const Array3& data)
	{
		m_ensembleName = ensembleName;
		m_fieldName = fieldName;
		m_configurationName = configurationName;
		m_cools = cools;
		m_data = data;
		m_slice = slice;
	}

	///
	/// \brief WriteFile
	/// \param filename
	/// \param data
	/// \return
	/// \since	19-11-2015
	/// \author	Dean
	///
	bool operator()(const std::string& filename) const
	{
		using namespace std;

		//	Output to file
		ofstream outFile(filename);
		if (outFile.is_open())
		{
			auto dimX = m_data.GetDimX();
			auto dimY = m_data.GetDimY();
			auto dimZ = m_data.GetDimZ();

			//	Compute stats
			auto minVal = (*min_element(m_data.cbegin(), m_data.cend()));
			auto maxVal = (*max_element(m_data.cbegin(), m_data.cend()));
			auto sum = std::accumulate(m_data.cbegin(), m_data.cend(), 0.0);
			auto average = sum / m_data.GetElementCount();

			outFile << m_ensembleName << endl;
			outFile << m_configurationName << endl;
			outFile << m_cools << endl;
			outFile << m_slice << endl;
			outFile << m_fieldName << endl;

			outFile << dimX << " " << dimY << " " << dimZ << endl;
			outFile << endl;

			outFile << minVal << endl;
			outFile << maxVal << endl;
			outFile << sum << endl;
			outFile << average << endl;

			for (auto x = 0; x < dimX; ++x)
			{
				for (auto y = 0; y < dimY; ++y)
				{
					for (auto z = 0; z < dimZ; ++z)
					{
						outFile << setw(4) << x << setw(4) << y << setw(4) << z << "\t";
						outFile << setw(24) << setprecision(20) << m_data.at(x, y, z);
						outFile << endl;
					}
				}
			}

			outFile.close();

			return true;
		}
		else return false;
	}
};

template <typename T>
struct Vol1Reader
{
	using value_type = T;
	Storage::Vector3<value_type> data;

	struct Header
	{
		//	... ensemble name ...
	};

	void operator()(const std::string& filename)
	{
		using namespace std;
		using value_type = float;

		//cout << __PRETTY_FUNCTION__ << endl;

		assert(filename != "");

		//HeightField3D result;
		string buffer;

		cout << "Opening file: " << filename << endl;

		ifstream inFile(filename);
		assert(inFile.is_open());

		if (!inFile.fail() && inFile.is_open())
		{
			//	Ensemble name
			getline(inFile, buffer);
			//result.SetEnsembleName(buffer);

			//	Configuration name
			getline(inFile, buffer);
			//result.SetConfigurationName(buffer);

			//	Next two lines should contain the cooling cycle and time step
			size_t coolingCycle;
			size_t timeStep;
			inFile >> std::skipws >> coolingCycle >> std::ws;
			inFile >> std::skipws >> timeStep >> std::ws;
			//result.SetCoolingValue(coolingCycle);
			//result.SetFixedVariableValue(timeStep);

			//	Next we should have the field variable (could contain spaces)
			std::getline(inFile, buffer);
			//result.SetVariableName(buffer);

			//	Now the field dimensions
			size_t dimX, dimY, dimZ;
			inFile >> std::skipws >> dimX >> dimY >> dimZ;
			assert((dimX > 0) && (dimY > 0) && (dimZ > 0));
			data.resize(dimX, dimY, dimZ);
			auto expectedVertexCount = dimX * dimY * dimZ;

			//	Next we should have a blank line
			std::getline(inFile, buffer);

			//	Next four values are computed stats)
			value_type minVal, maxVal, totVal, aveVal;
			inFile >> std::skipws >> minVal >> maxVal >> totVal >> aveVal;

			//	next load each sample
			size_t actualVertexCount = 0;
			while ((!inFile.eof()) && (actualVertexCount < expectedVertexCount))
			{
				//	Reading line by line
				size_t x, y, z;
				value_type value;

				//	Read the coordinates and the data
				inFile >> x >> y >> z;
				inFile >> value;

				//	Read in the values
				data.at(x, y, z) = value;

				//	Increment the counter
				++actualVertexCount;
			}

			inFile.close();

			//	Check that we read in the expected number of samples
			assert(actualVertexCount == expectedVertexCount);
		}
		else throw std::invalid_argument(std::string("Unable to open file: " + filename).c_str());
	}

};

#endif // VOL_1_H

