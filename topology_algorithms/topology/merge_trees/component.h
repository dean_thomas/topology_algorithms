#ifndef COMPONENT_H
#define COMPONENT_H

#include <iostream>
#include <sstream>
#include <string>
#include <cassert>

struct Component
{
	operator std::string() const;
	using HeightSample_ptr = float const*;
	using Component_ptr = Component*;

	HeightSample_ptr m_loEnd = nullptr;
	HeightSample_ptr m_hiEnd = nullptr;

	Component_ptr m_nextHi = nullptr;
	Component_ptr m_nextLo = nullptr;
	Component_ptr m_lastHi = nullptr;
	Component_ptr m_lastLo = nullptr;

	HeightSample_ptr m_seedFrom = nullptr;
	HeightSample_ptr m_seedTo = nullptr;

	Component_ptr m_ufComponent = nullptr;

	explicit Component(const HeightSample_ptr& endHiLo)
		: m_loEnd { endHiLo },
		  m_hiEnd { endHiLo }
	{
		m_nextHi = this;
		m_nextLo = this;
		m_lastHi = this;
		m_lastLo = this;

		m_seedFrom = nullptr;
		m_seedTo = nullptr;

		m_ufComponent = this;
	}

	static Component_ptr newJoinComponent(const HeightSample_ptr &endHiLo,
			  const Component_ptr& nextHi,
			  const Component_ptr& lastHi)
	{
		assert (endHiLo != nullptr);
		assert (nextHi != nullptr);
		assert (lastHi != nullptr);

		auto newComponent = new Component(endHiLo);
		assert(newComponent != nullptr);

		newComponent->m_loEnd = endHiLo;
		newComponent->m_hiEnd = endHiLo;
		newComponent->m_nextHi = nextHi;
		newComponent->m_lastHi = lastHi;

		newComponent->m_nextLo = newComponent;
		newComponent->m_lastLo = newComponent;

		newComponent->m_seedFrom = nullptr;
		newComponent->m_seedTo = nullptr;

		newComponent->m_ufComponent = newComponent;

		return newComponent;
	}

	static Component_ptr newSplitComponent(const HeightSample_ptr &endHiLo,
			  const Component_ptr& nextLo,
			  const Component_ptr& lastLo)
	{
		assert (endHiLo != nullptr);
		assert (nextLo != nullptr);
		assert (lastLo != nullptr);

		auto newComponent = new Component(endHiLo);
		assert(newComponent != nullptr);

		newComponent->m_loEnd = endHiLo;
		newComponent->m_hiEnd = endHiLo;
		newComponent->m_nextLo = nextLo;
		newComponent->m_lastLo = lastLo;

		newComponent->m_nextHi = newComponent;
		newComponent->m_lastHi = newComponent;

		newComponent->m_seedFrom = nullptr;
		newComponent->m_seedTo = nullptr;

		newComponent->m_ufComponent = newComponent;

		return newComponent;
	}

	void mergeTo(const Component_ptr newUF)
	{
		assert(newUF != nullptr);

		m_ufComponent = newUF;
	}

	void mergeDown(const Component_ptr joinComponent)
	{
		assert(joinComponent != nullptr);

		m_nextLo = joinComponent;
		m_lastLo = joinComponent->m_lastHi;

		m_lastLo->m_nextLo = this;

		m_ufComponent = joinComponent;

		joinComponent->m_lastHi = this;
	}

	//	Path compression
	Component* component()
	{
		using namespace std;
		auto depth = 0ul;

		while (m_ufComponent->m_ufComponent != m_ufComponent)
		{
			++depth;
			m_ufComponent = m_ufComponent->m_ufComponent;
		}
		cout << "Using path compression: depth = " << depth << endl;;


		return m_ufComponent;
	}
};

inline Component::operator std::string() const
{
	using namespace std;
	stringstream ss;

	ss << "Component" << endl;

	ss << "\tUnion-Find component: " <<
		  (m_ufComponent == nullptr ? "nullptr" : to_string(std::uintptr_t(m_ufComponent))) << endl;

	ss << "\tHi-end: " <<
		  (m_hiEnd == nullptr ? "nullptr" : to_string(std::uintptr_t(m_hiEnd))) << endl;
	ss << "\tLo-end: " <<
		  (m_loEnd == nullptr ? "nullptr" : to_string(std::uintptr_t(m_loEnd))) << endl;

	ss << "\tNext hi: " <<
		  (m_nextHi == nullptr ? "nullptr" : to_string(std::uintptr_t(m_nextHi))) << endl;
	ss << "\tLast hi: " <<
		  (m_lastHi == nullptr ? "nullptr" : to_string(std::uintptr_t(m_lastHi))) << endl;

	ss << "\tNext lo: " <<
		  (m_nextLo == nullptr ? "nullptr" : to_string(std::uintptr_t(m_nextLo))) << endl;
	ss << "\tLast lo: " <<
		  (m_lastLo == nullptr ? "nullptr" : to_string(std::uintptr_t(m_lastLo))) << endl;

	ss << "\tSeed from: " <<
		  (m_seedFrom == nullptr ? "nullptr" : to_string(std::uintptr_t(m_seedFrom))) << endl;
	ss << "\tSeed to: " <<
		  (m_seedTo == nullptr ? "nullptr" : to_string(std::uintptr_t(m_seedTo))) << endl;

	return ss.str();
}

inline std::ostream& operator<<(std::ostream& os, const Component& comp)
{
	using namespace std;

	os << static_cast<string>(comp);
	return os;
}

#endif // COMPONENT_H
