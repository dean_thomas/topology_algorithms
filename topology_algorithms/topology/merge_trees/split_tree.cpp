#include "split_tree.h"

#include <HeightField3D.h>


using namespace std;

SplitTree::SplitTree()
{

}

void SplitTree::createTree(HeightField3D* heightField)
{
	assert(heightField != nullptr);
	cout << "Creating join tree." << endl;

	//	Initial component need creating here so that we can access
	//	the last one out side of the main loop
	Component* splitComponent = nullptr;

	for (auto i = 0ul; i < heightField->GetVertexCount(); ++i)
	{
		//	B.i.
		cout << "B.i." << endl;

		//	Unlike the join tree we loop from the highest vertices
		auto v = i;
		auto currentHeight = heightField->GetSortedHeight(v);
		auto vertex3d = heightField->ComputeIndex(heightField->GetSortedHeight(v));
		vec3 pos3 = { vertex3d.x, vertex3d.y, vertex3d.z };
		cout << "Current vertex is: " << pos3 << " with index " << v << endl;

		//	Get a list of neighbours for the current vertex
		//	The split tree uses 18 neighbours and the join tree 6.
		std::deque<vec3> neighbours = queueNeighbours(heightField, pos3,
													  Neighbours::EIGHTEEN);
		cout << neighbours << endl;
		auto neighbourComponentCount = 0ul;

		//	Get the current split component (could well be nullptr)
		splitComponent = findComponent(pos3);
		cout << (splitComponent == nullptr
				 ? "Component is nullptr"
				 : static_cast<string>(*splitComponent)) << endl;

		for (auto n = 0ul; n < neighbours.size(); ++n)
		{
			//	Now we loop through the identified neighbours
			cout << "Component lookup: " << m_componentLookup << endl;

			auto current_neighbour = neighbours[n];
			vec3 neighbourPos3 = { current_neighbour.x,
								   current_neighbour.y,
								   current_neighbour.z };
			cout << "Inspecting neighbour " << neighbourPos3 << endl;

			//	Get a pointer to the neighbour sample
			auto neighbourHeight = &(heightField->GetHeightAt(neighbourPos3.x,
															  neighbourPos3.y,
															  neighbourPos3.z));

			//	If the neighbour sorts higher we simply skip the vertex
			//	Note: the split and join tree branch differently depending
			//	on the relative sorting of vertices.
			auto result = heightSort(neighbourHeight, currentHeight);
			cout << "Height sort result = " << result << endl;
			if (result > 0) continue;

			//	Obtain the component representing the neighbour.
			//	Note: here we use path compression to obtain the correct
			//	neighbour.
			Component* nbrComp = findComponent(neighbourPos3)->component();
			cout << (nbrComp == nullptr ? "Neighbour component is nullptr"
										: static_cast<string>(*nbrComp))
				 << endl;

			//	Compare the components
			if (splitComponent != nbrComp)
			{
				// B.ii.b.
				//	b.	if the neighbour belongs to a different component
				//		than the vertex...
				cout << "B.ii.b." << endl;

				if (neighbourComponentCount == 0)
				{
					//	B.ii.b.1.
					//	1.	...and the vertex has no component yet, add the
					//		vertex to the component
					cout << "B.ii.b.1" << endl;

					//	Set the component lookup for this vertex to point to the neighbour component
					m_componentLookup[pos3] = nbrComp;
					splitComponent = nbrComp;
					cout << "Set component at " << pos3 << " to " << *nbrComp << endl;

					//	Set the seeds to join the neighbour height and current vertex height
					nbrComp->m_seedTo = neighbourHeight;
					nbrComp->m_seedFrom = currentHeight;

					//	Create an arc between the neighbour and the current
					//	vertex
					auto temp = heightField->ComputeIndex(nbrComp->m_hiEnd);
					vec3 split3 = { temp.x, temp.y, temp.z };
					m_arcLookup[split3] = currentHeight;
					nbrComp->m_hiEnd = currentHeight;

					++neighbourComponentCount;
				}
				else if (neighbourComponentCount == 1)
				{
					// B.ii.b.2.
					//	2.	...the vertex is a component, but is not yet a
					//	split, 	make it a split
					cout << "B.ii.b.2" << endl;

					assert(currentHeight != nullptr);
					assert(neighbourHeight != nullptr);

					assert(splitComponent != nullptr);
					assert(nbrComp != nullptr);

					auto newComponent = Component::newSplitComponent(
								currentHeight, splitComponent, nbrComp);
					assert(newComponent != nullptr);

					//	set the seed pointer to nbr (it's adjacent to the
					//	vertex), but set both ends . . .
					nbrComp->m_seedTo = neighbourHeight;
					nbrComp->m_seedFrom = currentHeight;

					//	Create an arc between the neighbour
					//	using loEnd for join tree, hiEnd for split tree
					auto temp = heightField->ComputeIndex(nbrComp->m_hiEnd);
					vec3 split3 = { temp.x, temp.y, temp.z };

					//	Add the join arc to the tree
					m_arcLookup[split3] = currentHeight;

					//	point the neighbour component to this vertex
					//	working with the loEnd in the join tree and
					//	hiEnd in split tree
					nbrComp->m_hiEnd = currentHeight;

					//	For the join tree we set low-end pointers,
					//	for the split tree set high-end pointers
					nbrComp->m_nextHi = newComponent;
					nbrComp->m_lastHi = splitComponent;

					//	merge the components
					nbrComp->mergeTo(newComponent);
					splitComponent->mergeTo(newComponent);

					//	set the hi-end of the split component the current
					//	vertex (low end in join tree)
					splitComponent->m_hiEnd = currentHeight;

					//	set lo-end pointers for a join,
					//	high-end pointers for a split
					splitComponent->m_nextHi = nbrComp;
					splitComponent->m_lastHi = newComponent;

					//	redundant?  seems to be done above
					splitComponent = newComponent;

					m_componentLookup[pos3] = newComponent;
					cout << "Set component at " << pos3 << " to "
						 << *newComponent << endl;

					//	This will be a new supernode
					++m_criticalNodes;
					cout << "Critical node count is now "
						 << m_criticalNodes << endl;
					++neighbourComponentCount;
				}
				else
				{
					// B.ii.b.3
					//	3.	...the vertex is a split, merge the additional
					//	component
					cout << "B.ii.b.3" << endl;

					assert(currentHeight != nullptr);
					assert(neighbourHeight != nullptr);
					assert(splitComponent != nullptr);
					assert(nbrComp != nullptr);

					//	Create an arc between the neighbour
					auto temp = heightField->ComputeIndex(nbrComp->m_hiEnd);
					vec3 split3 = { temp.x, temp.y, temp.z };

					//	add to merge tree
					m_arcLookup[split3] = currentHeight;

					//	Set seeds for neighbour
					nbrComp->m_seedFrom = currentHeight;
					nbrComp->m_seedTo = neighbourHeight;

					//	Note: currently most of this is handled in the
					//	mergeDown method for the join tree
					nbrComp->m_hiEnd = currentHeight;
					nbrComp->m_nextHi = splitComponent;
					nbrComp->m_lastHi = splitComponent->m_lastLo;

					nbrComp->mergeTo(splitComponent);
					splitComponent->m_lastLo = nbrComp;
				}
			}
		}
		if (splitComponent == nullptr)
		{
			//	Start a new component - it must be a local minimum
			auto newComponent = new Component(currentHeight);
			assert(newComponent != nullptr);
			splitComponent = newComponent;

			//	Set the component array for this vertex to the new component
			m_componentLookup[pos3] = newComponent;
			cout << "Set component at " << pos3 << " to " << *newComponent << endl;

			//	This will be a new supernode
			++m_criticalNodes;
			cout << "Supernode count is now " << m_criticalNodes << endl;
		}
	}

	//	The final component needs to tied off at plus infinity
	assert(splitComponent != nullptr);
	auto temp = heightField->ComputeIndex(splitComponent->m_hiEnd);
	vec3 split3 = { temp.x, temp.y, temp.z };
	m_arcLookup[split3] = PLUS_INFINITY_PTR;
	splitComponent->m_hiEnd = PLUS_INFINITY_PTR;
	splitComponent->m_nextHi = splitComponent;
	splitComponent->m_lastHi = splitComponent;

	//	Root the tree
	m_rootComponent = splitComponent;
}
