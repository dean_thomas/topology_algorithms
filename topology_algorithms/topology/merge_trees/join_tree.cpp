#include "join_tree.h"

#include <HeightField3D.h>


using namespace std;

JoinTree::JoinTree()
{

}

void JoinTree::createTree(HeightField3D* heightField)
{
	assert(heightField != nullptr);
	cout << "Creating join tree." << endl;

	//	Initial component need creating here so that we can access
	//	the last one out side of the main loop
	Component* jComp = nullptr;

	for (auto i = 0ul; i < heightField->GetVertexCount(); ++i)
	{
		//	B.i.
		cout << "B.i." << endl;

		//	Unlike the split tree we loop from the lowest vertices
		auto v = heightField->GetVertexCount() - 1 - i;
		auto currentHeight = heightField->GetSortedHeight(v);
		auto vertex3d = heightField->ComputeIndex(heightField->GetSortedHeight(v));
		vec3 pos3 = { vertex3d.x, vertex3d.y, vertex3d.z };
		cout << "Current vertex is: " << pos3 << " with index " << v << endl;

		//	Get a list of neighbours for the current vertex
		//	The split tree uses 18 neighbours and the join tree 6.
		std::deque<vec3> neighbours = queueNeighbours(heightField, pos3,
													  Neighbours::SIX);
		cout << neighbours << endl;
		auto neighbourComponentCount = 0ul;

		//	Get the current join component (could well be nullptr)
		jComp = findComponent(pos3);
		cout << (jComp == nullptr ? "Component is nullptr"
								  : static_cast<string>(*jComp)) << endl;

		for (auto n = 0ul; n < neighbours.size(); ++n)
		{
			//	Now we loop through the identified neighbours
			cout << "Component lookup: " << m_componentLookup << endl;

			auto current_neighbour = neighbours[n];
			vec3 neighbourPos3 = { current_neighbour.x, current_neighbour.y, current_neighbour.z };
			cout << "Inspecting neighbour " << neighbourPos3 << endl;

			//	Get a pointer to the neighbour sample
			auto neighbourHeight = &(heightField->GetHeightAt(neighbourPos3.x, neighbourPos3.y, neighbourPos3.z));

			//	If the neighbour sorts lower we simply skip the vertex
			//	Note: the split and join tree branch differently depending
			//	on the relative sorting of vertices.
			auto result = heightSort(neighbourHeight, currentHeight);
			cout << "Height sort result = " << result << endl;
			if (result < 0) continue;

			//	Obtain the component representing the neighbour.
			//	Note: here we use path compression to obtain the correct
			//	neighbour.
			Component* nbrComp = findComponent(neighbourPos3)->component();
			cout << (nbrComp == nullptr ? "Neighbour component is nullptr"
										: static_cast<string>(*nbrComp))
				 << endl;

			//	Compare the components
			if (jComp != nbrComp)
			{
				// B.ii.b.
				cout << "B.ii.b." << endl;

				if (neighbourComponentCount == 0)
				{
					// B.ii.b.1.
					cout << "B.ii.b.1" << endl;

					//	components could be null when starting this step
					//assert(jComp != nullptr);
					//assert(nbrComp != nullptr);

					//	Set the component lookup for this vertex to point to
					//	the neighbour component
					m_componentLookup[pos3] = nbrComp;
					jComp = nbrComp;
					cout << "Set component at " << pos3 << " to "
						 << *nbrComp << endl;

					//	Set the seeds to join the neighbour height and current
					//	vertex height
					nbrComp->m_seedTo = neighbourHeight;
					nbrComp->m_seedFrom = currentHeight;

					//	Create an arc between the neighbour
					auto temp = heightField->ComputeIndex(nbrComp->m_loEnd);
					vec3 join3 = { temp.x, temp.y, temp.z };
					m_arcLookup[join3] = currentHeight;
					nbrComp->m_loEnd = currentHeight;

					++neighbourComponentCount;
				}
				else if (neighbourComponentCount == 1)
				{
					// B.ii.b.2.
					cout << "B.ii.b.2" << endl;

					assert(currentHeight != nullptr);
					assert(neighbourHeight != nullptr);

					assert(jComp != nullptr);
					assert(nbrComp != nullptr);

					auto newComponent = Component::newJoinComponent(
								currentHeight, jComp, nbrComp);
					assert(newComponent != nullptr);

					//	set the seed pointer to nbr (it's adjacent to the
					//	vertex), but set both ends . . .
					nbrComp->m_seedTo = neighbourHeight;
					nbrComp->m_seedFrom = currentHeight;

					//	Create an arc between the neighbour
					//	using loEnd for join tree, hiEnd for split tree
					auto temp = heightField->ComputeIndex(nbrComp->m_loEnd);
					vec3 join3 = { temp.x, temp.y, temp.z };

					//	Add the join arc to the tree
					m_arcLookup[join3] = currentHeight;

					//	point the neighbour component to this vertex
					//	working with the loEnd in the join tree and
					//	hiEnd in split tree
					nbrComp->m_loEnd = currentHeight;

					//	For the join tree we set low-end pointers,
					//	for the split tree set high-end pointers
					nbrComp->m_nextLo = newComponent;
					nbrComp->m_lastLo = jComp;

					//	set the lo-end of the join component the current
					//	vertex (high end in split tree)
					jComp->m_loEnd = currentHeight;

					//	set lo-end pointers for a join,
					//	high-end pointers for a split
					jComp->m_nextLo = nbrComp;
					jComp->m_lastLo = newComponent;

					//	merge the components
					nbrComp->mergeTo(newComponent);
					jComp->mergeTo(newComponent);

					//	redundant?  seems to be done above
					jComp = newComponent;

					m_componentLookup[pos3] = newComponent;
					cout << "Set component at " << pos3 << " to "
						 << *newComponent << endl;

					//	This will be a new supernode
					++m_criticalNodes;
					cout << "Critical node count is now "
						 << m_criticalNodes << endl;
					++neighbourComponentCount;
				}
				else
				{
					// B.ii.b.3
					//	3.	...the vertex is a join, merge the additional
					//	component
					cout << "B.ii.b.3" << endl;

					assert(currentHeight != nullptr);
					assert(neighbourHeight != nullptr);
					assert(jComp != nullptr);
					assert(nbrComp != nullptr);

					//	Create an arc between the neighbour
					auto temp = heightField->ComputeIndex(nbrComp->m_loEnd);
					vec3 join3 = { temp.x, temp.y, temp.z };

					//	add to merge tree
					m_arcLookup[join3] = currentHeight;

					//	Set seeds for neighbour
					nbrComp->m_seedFrom = currentHeight;
					nbrComp->m_seedTo = neighbourHeight;

					nbrComp->m_loEnd = currentHeight;
					nbrComp->mergeDown(jComp);
				}
			}
		}
		if (jComp == nullptr)
		{
			//	Start a new component - it must be a local maximum
			auto newComponent = new Component(currentHeight);
			assert(newComponent != nullptr);
			jComp = newComponent;

			//	Set the component array for this vertex to the new component
			m_componentLookup[pos3] = newComponent;
			cout << "Set component at " << pos3 << " to " << *newComponent << endl;

			//	This will be a new supernode
			++m_criticalNodes;
			cout << "Supernode count is now " << m_criticalNodes << endl;
		}
	}

	//	The final component needs to tied off at minus infinity
	assert(jComp != nullptr);
	auto temp = heightField->ComputeIndex(jComp->m_loEnd);
	vec3 join3 = { temp.x, temp.y, temp.z };
	m_arcLookup[join3] = MINUS_INFINITY_PTR;
	jComp->m_loEnd = MINUS_INFINITY_PTR;
	jComp->m_nextLo = jComp;
	jComp->m_lastLo = jComp;

	//	Root the tree
	m_rootComponent = jComp;
}
