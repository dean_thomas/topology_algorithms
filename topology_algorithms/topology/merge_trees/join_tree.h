#ifndef JOIN_TREE_H
#define JOIN_TREE_H

#include "merge_tree.h"

class JoinTree : public MergeTree
{
public:
	JoinTree();

	void createTree(HeightField3D *heightField);

	std::string ToDotString(HeightField3D *heightField) const
	{
		return MergeTree::ToDotString(heightField, false);
	}
};



#endif // JOIN_TREE_H
