#include "HeightSort.h"
#include "TypeDefines.h"

#include <iostream>

//	compareHeightVoid():	compares two heights in an array
int compareHeightVoid(const void *e1, const void *e2)								//	comparison function for sorting, &c.
{
	Real **d1 = (Real **) e1; Real **d2 = (Real **) e2;						//	convert to correct type
	return compareHeight(*d1, *d2);											//	and call the comparison function
} // end of compareHeightVoid()

//	compareHeight():		compares two heights in an array
int compareHeight(const Real *d1, const Real *d2)								//	comparison function for all other purposes
{
	using std::cout;
	using std::cerr;
	using std::endl;
	using std::hex;

	cout << "Height A: " << *d1 << " Height B: " << *d2 << endl;

	if (*d1 < *d2) return -1;												//	and compute return values
	if (*d1 > *d2) return 1;
	if (d1 < d2) return -1;													//	break ties with pointer addresses (unique)
	if (d1 > d2) return 1;

	cerr << "Major problem:  neither sorted higher." << endl;
	cerr << hex << d1 << "(" << *d1 << ")" << endl;
	cerr << hex << d2 << "(" << *d2 << ")" << endl;

	return 0;
} // end of compareHeight()


