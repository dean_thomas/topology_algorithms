#ifndef VEC3_H
#define VEC3_H

#include <iostream>

/**
 * @brief Defines a basic tuple of 3 integers used to define integer
 * coordinates
 */
struct vec3
{
	size_t x;
	size_t y;
	size_t z;
};

inline bool operator==(const vec3& a, const vec3& b)
{
	return ((a.x == b.x) && (a.y == b.y) && (a.z == b.z));
}

inline std::ostream& operator<<(std::ostream& os, const vec3& vec)
{
	os << "{ "
	   << vec.x << ", "
	   << vec.y << ", "
	   << vec.z
	   << " }";
	return os;
}

namespace std
{
	template <>
	struct hash<vec3>
	{
		size_t operator ()(const vec3& vec) const
		{
			return ((vec.x * 73856093u) ^ (vec.y * 19349669u) ^ (vec.z * 83492791u));
		}
	};
}


#endif // VEC3_H
