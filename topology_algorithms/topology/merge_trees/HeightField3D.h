//	IsoSurface II
//	Hamish Carr, 1999

//	HeightField.h:		class representing the height field
//	Palatino-12; 5 n-spaces/tab

#ifndef HEIGHTFIELD_H
#define HEIGHTFIELD_H 1

#include <fstream>
#include <queue>
#include <string>
#include <sstream>
#include <cstdlib>
#include <memory>

//#include "Globals/TypeDefines.h"
#include <Vector3.h>


//  Forward declarations of Contour Tree and data model for later
class ContourTree;
class DataModel;
class NeighbourQueue;
class ReebGraph;

class HeightField3D
{
	using Real = float;

	public:
		using value_type = float;
		using size_type = size_t;
		using Array3f = Storage::Vector3<value_type>;
		using Container = Array3f;
		using Index3d = Storage::Index3;

		using iterator = Container::iterator;
		using const_iterator = Container::const_iterator;
		using reverse_iterator = Container::reverse_iterator;
		using const_reverse_iterator = Container::const_reverse_iterator;

		iterator begin() { return m_data.begin(); }
		const_iterator begin() const { return m_data.begin(); }
		const_iterator cbegin() const noexcept { return m_data.cbegin(); }
		iterator end() { return m_data.end(); }
		const_iterator end() const { return m_data.end(); }
		const_iterator cend() const noexcept { return m_data.cend(); }

		reverse_iterator rbegin() { return m_data.rbegin(); }
		const_reverse_iterator rbegin() const { return m_data.rbegin(); }
		const_reverse_iterator crbegin() const noexcept { return m_data.crbegin(); }
		reverse_iterator rend() { return m_data.rend(); }
		const_reverse_iterator rend() const { return m_data.rend(); }
		const_reverse_iterator crend() const noexcept { return m_data.crend(); }

		using AxisLabelType = char;
		using RealArray2d = Storage::Vector2<value_type>;
		using Array2dList = std::vector<RealArray2d>;

		enum class FileType
		{
			TXT		=	0,
			VOL		=	1,
			VOL1	=	2,
			VOL3	=	3,
			UNKNOWN	=	255
		};

		enum class Boundary
		{
			MIN_X	=	0,
			MIN_Y	=	1,
			MIN_Z	=	2,
			MAX_X	=	3,
			MAX_Y	=	4,
			MAX_Z	=	5,
		};

		//	====================================================================
		//	Member variables
		//	====================================================================
	private:
		ReebGraph* m_reebGraph = nullptr;

		Array2dList m_xySlices;
		Array2dList m_yzSlices;
		Array2dList m_xzSlices;

		AxisLabelType m_axisLabelX;
		AxisLabelType m_axisLabelY;
		AxisLabelType m_axisLabelZ;

		FileType parseFiletype(const char* filename) const;

		//	Link back to the master DataModel
		DataModel *m_dataModel = nullptr;
		//	Each data file will have an individual (2D) contour tree
		ContourTree* m_contourTree  = nullptr;

		//	Properties of the simulation
		std::string m_ensembleName;
		std::string m_configurationName;
		std::string m_fieldName;
		std::string m_fixedVariableName;

		size_t m_coolingSlice = 0;
		unsigned long m_fixedVariable;

		float m_mu;

		bool m_isBusy;
		bool m_isLoaded;
		bool m_contourTreeIsReady;

		bool m_periodicBoundaries;

		//	1-D array for sorting the heights
		Real **m_sortedHeights = NULL;

		//	maximum & minimum height value
		Real m_maxHeight;
		Real m_minHeight;

		//	3-D array holding the height values
		//	TODO: keep a second heightfield array for transformed data
		Container m_data;

		//	total # of vertices
		//unsigned long m_vertexCount;

		//	For keeping track of lattice coordinate tranformations
		//unsigned long m_translationX;
		//unsigned long m_transltaionY;
		//unsigned long m_translationZ;

		//	this one is a hack for computing correct Riemann sums
		Real m_sampleSum;

		unsigned long findMatchingFacePolygons();
		//	====================================================================
		//	Const functions
		//	====================================================================
		//
	public:
		void SetReebGraph(ReebGraph* const reebGraph)
		{
			assert(reebGraph != nullptr);
			m_reebGraph = reebGraph;
		}

		ReebGraph* GetReebGraph() const { return m_reebGraph; }

		static std::string GenerateCacheFilename(const std::string& fieldName,
												 const std::string& variableName,
												 const size_type& variableVal);
		std::string GetCacheFilename() const;

		void SetAxisLabelX(const AxisLabelType &value) { m_axisLabelX = value; }
		AxisLabelType GetAxisLabelX() const { return m_axisLabelX; }

		void SetAxisLabelY(const AxisLabelType &value) { m_axisLabelY = value; }
		AxisLabelType GetAxisLabelY() const { return m_axisLabelY; }

		void SetAxisLabelZ(const AxisLabelType &value) { m_axisLabelZ = value; }
		AxisLabelType GetAxisLabelZ() const { return m_axisLabelZ; }

		void SetAxisLabels(const AxisLabelType &xAxis,
						   const AxisLabelType &yAxis,
						   const AxisLabelType &zAxis)
		{
			m_axisLabelX = xAxis;
			m_axisLabelY = yAxis;
			m_axisLabelZ = zAxis;
		}

		//	example: "config.b190k1680mu0000j00s12t24"
		void SetEnsembleName(const std::string& value) { m_ensembleName = value; }
		std::string GetEnsembleName() const { return m_ensembleName; }

		//	example: "Topological charge density"
		void SetFieldName(const std::string &value) { m_fieldName = value; }
		std::string GetFieldName() const { return m_fieldName; }

		//	example: "conp0050"
		void SetConfigurationName(const std::string &value) { m_configurationName = value; }
		std::string GetConfigurationName() const { return m_configurationName; }

		//	example: "time"
		void SetFixedVariableName(const std::string &value) { m_fixedVariableName = value; }
		std::string GetFixedVariableName() const { return m_fixedVariableName; }

		//	example: 5
		void SetCoolingSlice(const size_type value) { m_coolingSlice = value; }
		size_t GetCoolingSlice() const { return m_coolingSlice; }

		//	example: 0
		void SetFixedVariableValue(const unsigned long &value) { m_fixedVariable = value; }
		unsigned long GetFixedVariableValue() const { return m_fixedVariable; }

		bool IsLoaded() const;
		bool IsBusy() const { return m_isBusy; }
		bool ContourTreeIsReady() const { return m_contourTreeIsReady; }

		unsigned long GetVertexCount() const;

		Real *GetSortedHeight(const unsigned long index) const;

		DataModel* GetDataModel() const;

		void SetHeightAt(const size_type x,
						 const size_type y,
						 const size_type z,
						 const value_type& value);

		value_type GetHeightAt(const unsigned long x,
						 const unsigned long y,
						 const unsigned long z) const;

		value_type &GetHeightAt(const unsigned long x,
						  const unsigned long y,
						  const unsigned long z);

		Index3d ComputeIndex(const Real *element) const;
		void ComputeIndex(const Real *element,
						  size_t &x,
						  size_t &y,
						  size_t &z) const;

		void ResetLocalContours(const Real value);


		Real GetMaxHeight() const;
		Real GetMinHeight() const;

		std::string ToString() const;

		//	Returns the dimensions of the original data set
		unsigned long GetBaseDimX() const { return m_data.GetDimX(); }
		unsigned long GetBaseDimY() const { return m_data.GetDimY(); }
		unsigned long GetBaseDimZ() const { return m_data.GetDimZ(); }

		//	Returns the maximum on each axis (so if DimX = 12, values run
		//	0..11)
		Real GetMaxX() const { return m_data.GetDimX() - 1.0; }
		Real GetMaxY() const { return m_data.GetDimY() - 1.0; }
		Real GetMaxZ() const { return m_data.GetDimZ() - 1.0; }

		//	Hack: temp accessor to data
		Container GetData() const { return m_data; }

		//	Translation of the lattice
		unsigned long GetTranslationX() const {
			return m_data.GetTranslationX(); }
		unsigned long GetTranslationY() const {
			return m_data.GetTranslationY(); }
		unsigned long GetTranslationZ() const {
			return m_data.GetTranslationZ(); }

		//	Set offset of heightfield to a specific value
		void SetTranslation(const unsigned long x,
							const unsigned long y,
							const unsigned long z);



		unsigned long LargestDim() const;


		Real GetSampleSum() const;

		//	====================================================================
		//	Non-Const functions
		//	====================================================================
		//
	public:
		bool SaveToFile(const std::string& filename) const;

		Array3f GetUntransformedHeightArray() const
		{ return m_data; }

		ContourTree *GetContourTree();

		//	functions to transpose the data on the lattice
		unsigned long TranslatePosX(const bool recalculateContourTree = true);
		unsigned long TranslateNegX(const bool recalculateContourTree = true);

		unsigned long TranslatePosY(const bool recalculateContourTree = true);
		unsigned long TranslateNegY(const bool recalculateContourTree = true);

		unsigned long TranslatePosZ(const bool recalculateContourTree = true);
		unsigned long TranslateNegZ(const bool recalculateContourTree = true);

		bool LoadFromASCII(const char *fileName);
		bool LoadFromRaw(const char *filename,
						 const unsigned int xDim,
						 const unsigned int yDim,
						 const unsigned int zDim,
						 const unsigned int res);
		bool LoadFromVol1(const std::string &filename);

		void GenerateGhostCells();

		bool BuildContourTreeAndGenerateInitialContours();
	private:
		void sortByHeight();

		Array3f createHeightArrayWithGhostVerticesOnBothSides(
				const Array3f input) const;
		static Array3f createHeightArrayWithGhostVerticesOnFarSide(
				const Array3f input,
				const unsigned long verticesToClone = 1);
		static HeightField3D *CreateHeightField3dWithGhostVerticesOnFarSide(const HeightField3D* input);
	public:

		RealArray2d GetPlaneYZ(const size_t& x);
		RealArray2d GetPlaneXZ(const size_t& y);
		RealArray2d GetPlaneXY(const size_t& z);

		HeightField3D() : HeightField3D(nullptr) { }

		//	constructor:		constructs the object
		HeightField3D(DataModel *dataModel);

		bool LoadFromFile(const std::string& filename,
						  const bool generateGhostCells,
						  const bool createContourTree);
		bool LoadFromData(const HeightField3D::Array3f& data,
										 const bool createContourTree);

		//	constructor

		//	destructor:			destroys the object, and deallocates dynamically allocated storage (including height array)
		~HeightField3D();														//	destructor

		//	Does the slicing across a specific axis
		void sliceIntoPlanesXY(const bool &);
		void sliceIntoPlanesYZ(const bool &);
		void sliceIntoPlanesXZ(const bool &);

		//	routine to find seeds for edges that do not otherwise have them
		Real *findDescendingNeighbour(Real *theVertex,
									  Array3f heightArray);						//	find a descending neighbour. Any descending neighbour

		void clearSliceCaches();
	public:
		void SetDimensions(const size_type x, const size_type y, const size_type z);

		void SetData(Array3f data);

		//	debug routines
		std::string GetSortedHeightsAsString() const;												//	prints out the heights in sorted order

		void SaveSelectedArcsToOBJ(const std::string) const;

		Real ConvertNormalisedIsovalueToActualHeight(const Real& isovalue) const;
		Real ConvertActualHeightToNormalisedIsovalue(const Real& height) const;
}; // end of class HeightField

#endif
