#ifndef HEIGHTSORT_H
#define HEIGHTSORT_H

#include <cstdlib>
#include <cstdio>
#include "TypeDefines.h"

//	we start by defining a function for use when comparing heights:  this is passed to qsort(), and used any time we need to compare heights
//	thus, it includes the symbolic perturbation scheme

//	compareHeightVoid():	compares two heights in an array
int compareHeightVoid(const void *e1, const void *e2);							//	comparison function for passing to qsort()
//	compareHeight():		compares two heights in an array
int compareHeight(const Real *d1, const Real *d2);							//	comparison function for all other purposes


#endif // HEIGHTSORT_H
