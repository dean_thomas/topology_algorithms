#ifndef VECTOR_3_H
#define VECTOR_3_H


//	If enabled it will use the old 1d flattening algorithm (as in Hamish's
//	implementation).
#define USE_OLD_INDEX_ORDERING

#include <string>
#include <sstream>

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <iostream>

#include <Vector2.h>

//  Work around for VS2013
#if defined(_MSC_VER) && (_MSC_VER < 1900)
#define noexcept _NOEXCEPT
#endif

namespace Storage
{
    ///
    /// \brief	Allows indices to be passed as 3-tuples
    /// \since	24-09-2015
    /// \author	Dean
    ///
    struct Index3
    {
		size_t x;
	    size_t y;
	    size_t z;
    };

    template <typename T>
    class Vector3
    {
	public:
	    using Container = std::vector<T>;

	    using value_type = T;
	    using size_type = typename Container::size_type;
	    using reference = typename Container::reference;
	    using const_reference = typename Container::const_reference;

	    using iterator = typename Container::iterator;
	    using const_iterator = typename Container::const_iterator;
	    using reverse_iterator = typename Container::reverse_iterator;
	    using const_reverse_iterator = typename Container::const_reverse_iterator;

	    iterator begin() { return m_data.begin(); }
	    const_iterator begin() const { return m_data.begin(); }
	    const_iterator cbegin() const noexcept { return m_data.cbegin(); }
	    iterator end() { return m_data.end(); }
	    const_iterator end() const { return m_data.end(); }
	    const_iterator cend() const noexcept { return m_data.cend(); }

	    reverse_iterator rbegin() { return m_data.rbegin(); }
	    const_reverse_iterator rbegin() const { return m_data.rbegin(); }
	    const_reverse_iterator crbegin() const noexcept { return m_data.crbegin(); }
	    reverse_iterator rend() { return m_data.rend(); }
	    const_reverse_iterator rend() const { return m_data.rend(); }
	    const_reverse_iterator crend() const noexcept { return m_data.crend(); }

	    //	Structs and type aliases
	    using Index1 = size_type;

	    bool empty() const noexcept { return m_data.empty(); }
	    size_type size() const noexcept { return m_data.size(); }
	private:
	    //	the block of elements
	    Container m_data;

	    //	dimensions in each direction
	    size_type m_xDim = 0;
	    size_type m_yDim = 0;
	    size_type m_zDim = 0;

	    size_type m_translationX = 0;
	    size_type m_translationY = 0;
	    size_type m_translationZ = 0;

#ifdef USE_OLD_INDEX_ORDERING
	    size_type rowSize() const {return m_zDim; }
	    size_type sliceSize() const { return m_zDim * m_yDim; }
#else
	    size_type rowSize() const {return m_xDim; }
	    size_type sliceSize() const { return m_xDim * m_yDim; }
#endif

	    size_type nElements() const { return m_data.size(); }
	public:
	    size_type GetTranslationX() const { return m_translationX; }
	    size_type GetTranslationY() const { return m_translationY; }
	    size_type GetTranslationZ() const { return m_translationZ; }
	    size_type GetDimX() const { return m_xDim; }
	    size_type GetDimY() const { return m_yDim; }
	    size_type GetDimZ() const { return m_zDim; }

	    size_type GetElementCount() const { return nElements(); }

	    ///
	    /// \brief	allows resizing of the array
	    /// \param x
	    /// \param y
	    /// \param z
	    /// \author	    Dean
	    /// \since	    15-01-2016
	    ///
	    void resize(const size_type x, const size_type y, const size_type z)
	    {
		m_xDim = x;
		m_yDim = y;
		m_zDim = z;

		m_data.resize(x * y * z);
	    }

	    ///
	    /// \brief	allows resizing of the array - initializing elements
	    ///		with the specified value
	    /// \param x
	    /// \param y
	    /// \param z
	    /// \param va
	    /// \author	    Dean
	    /// \since	    15-01-2016
	    ///
	    void resize(const size_type x, const size_type y, const size_type z, const value_type& val)
	    {
		m_xDim = x;
		m_yDim = y;
		m_zDim = z;

		m_data.resize(x * y * z, val);
	    }

	    ///
	    /// \brief		Assignment operator
	    /// \param		rhs
	    /// \return		a reference to this
	    /// \since
	    /// \author		Dean
	    ///	\author		Dean: return a reference to this and check for self
	    ///				assignment [23-09-2015]
	    ///
	    Vector3<T>& operator =(const Vector3<T> &rhs)
	    {
		if (this != &rhs)
		{
		    m_xDim = rhs.m_xDim;
		    m_yDim = rhs.m_yDim;
		    m_zDim = rhs.m_zDim;

		    m_translationX = rhs.m_translationX;
		    m_translationY = rhs.m_translationY;
		    m_translationZ = rhs.m_translationZ;

		    m_data.resize(rhs.m_data.size());

		    for (unsigned long i = 0; i < rhs.GetElementCount(); ++i)
		    {
			m_data[i] = rhs.m_data[i];
		    }
		}
		return *this;
	    }

	    ///
	    /// \brief		Copy constructor
	    /// \param		rhs
	    /// \since
	    /// \author		Dean
	    ///
	    Vector3<T>(const Vector3<T> &rhs)
	    {
		m_xDim = rhs.m_xDim;
		m_yDim = rhs.m_yDim;
		m_zDim = rhs.m_zDim;

		m_translationX = rhs.m_translationX;
		m_translationY = rhs.m_translationY;
		m_translationZ = rhs.m_translationZ;

		m_data.resize(rhs.m_data.size());

		for (unsigned long i = 0; i < rhs.GetElementCount(); ++i)
		{
		    m_data[i] = rhs.m_data[i];
		}
	    }

	    ///
	    /// \brief		Constructor that sets the dimensions of the array and
	    ///				initializes with empty values
	    /// \param		xDim
	    /// \param		yDim
	    /// \param		zDim
	    /// \since		24-09-2015
	    /// \author		Dean
	    ///
	    Vector3<T>(const size_t& xDim, const size_t& yDim, const size_t& zDim)
	    {
		m_xDim = xDim;
		m_yDim = yDim;
		m_zDim = zDim;

		m_translationX = 0;
		m_translationY = 0;
		m_translationZ = 0;

		m_data.resize(xDim * yDim * zDim);

		for (unsigned long i = 0; i < nElements(); ++i)
		{
		    m_data[i] = {};
		}
	    }

	    ///
	    /// \brief		Default constructor
	    /// \since
	    /// \author		Dean
	    ///
	    Vector3<T>()
	    {
		m_xDim = 0;
		m_yDim = 0;
		m_zDim = 0;

		m_translationX = 0;
		m_translationY = 0;
		m_translationZ = 0;
	    }

	    ///
	    ///	\brief		Destructor
	    /// \since
	    /// \author		Dean
	    ///
	    ~Vector3<T>()
	    {

	    }

	    ///
	    /// \brief		use function call to access array elements
	    /// \param x
	    /// \param y
	    /// \param z
	    /// \return
	    /// \since
	    /// \author		Dean
	    /// \author		Dean: change bounds checking from exceptions to
	    ///				asserts [23-09-2015]
	    ///
	    reference operator() (const size_t& x,
				  const size_t& y,
				  const size_t& z)
	    {
		assert (x < m_xDim);
		assert (y < m_yDim);
		assert (z < m_zDim);

		return m_data[index3toIndex1(x,y,z)];
	    }

	    ///
	    /// \brief		use function call to access array elements
	    /// \param x
	    /// \param y
	    /// \param z
	    /// \return
	    /// \since
	    /// \author		Dean
	    /// \author		Dean: change bounds checking from exceptions to
	    ///				asserts [23-09-2015]
	    ///
	    const_reference operator() (const size_t& x,
					const size_t& y,
					const size_t& z) const
	    {
		assert (x < m_xDim);
		assert (y < m_yDim);
		assert (z < m_zDim);

		return m_data[index3toIndex1(x,y,z)];
	    }

	    ///
	    /// \brief		Returns a reference to the object with the specified
	    ///				x, y and z indices
	    /// \param x
	    /// \param y
	    /// \param z
	    /// \return
	    /// \since		24-09-2015
	    /// \author		Dean
	    ///
	    reference at(const size_t& x, const size_t& y, const size_t& z)
	    {
		assert (x < m_xDim);
		assert (y < m_yDim);
		assert (z < m_zDim);

		return m_data.at(index3toIndex1(x,y,z));
	    }

	    ///
	    /// \brief		Returns a constant reference to the object with the
	    ///				specified x, y and z indices
	    /// \param x
	    /// \param y
	    /// \param z
	    /// \return
	    /// \since		24-09-2015
	    /// \author		Dean
	    ///
	    const_reference at(const size_t& x, const size_t& y, const size_t& z) const
	    {
		assert (x < m_xDim);
		assert (y < m_yDim);
		assert (z < m_zDim);

		return m_data.at(index3toIndex1(x,y,z));
	    }

	    ///
	    /// \brief		Returns a reference to the object with the specified
	    ///				x, y and z indices
	    /// \param x
	    /// \param y
	    /// \param z
	    /// \return
	    /// \since		24-09-2015
	    /// \author		Dean
	    ///
	    reference at(const Index3& index3d)
	    {
		assert (index3d.x < m_xDim);
		assert (index3d.y < m_yDim);
		assert (index3d.z < m_zDim);

		return m_data.at(index3toIndex1(index3d.x,
						index3d.y,
						index3d.z));
	    }

	    ///
	    /// \brief		Returns a constant reference to the object with the
	    ///				specified x, y and z indices
	    /// \param x
	    /// \param y
	    /// \param z
	    /// \return
	    /// \since		24-09-2015
	    /// \author		Dean
	    ///
	    const_reference at(const Index3& index3d) const
	    {
		assert (index3d.x < m_xDim);
		assert (index3d.y < m_yDim);
		assert (index3d.z < m_zDim);

		return m_data.at(index3toIndex1(index3d.x,
						index3d.y,
						index3d.z));
	    }

	    ///
	    /// \brief		Calculates a 3-dimensional index from a 1-dimensinoal
	    ///				index
	    /// \param index
	    /// \return
	    /// \since		14-09-2015
	    /// \author		Dean
	    /// \author		Dean: added bounds checking assert [23-09-2015]
	    ///
	    Index3 index1ToIndex3(const Index1 &index) const
	    {
		assert (index < nElements());

#ifdef USE_OLD_INDEX_ORDERING
		//	Old format (Hamish's code)
		Index3 result;

		unsigned long pos = index;
		result.z = pos % m_zDim;
		pos = (pos - result.z) / m_zDim;
		result.y = pos % m_yDim;
		result.x = (pos - result.y) / m_yDim;

		return result;
#else
		//	New format
		Index3 result;

		result.x = index % m_xDim;
		result.y = ((index - result.x) / m_xDim) % m_yDim;
		result.z = ((index - result.y * m_xDim - result.x)
			    / (m_xDim * m_yDim)) % m_zDim;

		return result;
#endif
	    }

	    ///
	    /// \brief		Calculates a 1-dimensional index from a set of
	    ///				3-dimensinoal indices
	    /// \return
	    /// \since		14-09-2015
	    /// \author		Dean
	    ///
	    Index1 index3toIndex1(const size_type& x,
				  const size_type& y,
				  const size_type& z) const
	    {
		assert (x < m_xDim);
		assert (y < m_yDim);
		assert (z < m_zDim);

#ifdef USE_OLD_INDEX_ORDERING
		//	Old format (Hamish's code)
		return z + rowSize() * y + sliceSize() * x;
#else
		//	New format
		return x + rowSize() * y + sliceSize() * z;
#endif
	    }

	    ///
	    /// \brief GetLargestDim
	    /// \return
	    ///	\since
	    /// \author		Dean
	    size_type GetLargestDim() const
	    {
		if (m_xDim > m_yDim)
		{
		    if (m_xDim > m_zDim)
		    {
			return m_xDim;
		    }
		    else
		    {
			return m_zDim;
		    }
		}
		else
		{
		    if (m_yDim > m_zDim)
		    {
			return m_yDim;
		    }
		    else
		    {
			return m_zDim;
		    }
		}
	    }

	    ///
	    /// \brief		computes position & returns it in X, Y, Z
	    /// \param element
	    /// \param X
	    /// \param Y
	    /// \param Z
	    /// \since
	    /// \author		Hamish
	    ///
	    void ComputeIndex(const T *element,
			      size_t &X,
			      size_t &Y,
			      size_t &Z) const
	    {
		assert (element != nullptr);

		//	find the offset from the base of the array
		size_t index = element - &m_data[0];

		assert (index < nElements());
		Index3 temp = index1ToIndex3(index);

		X = temp.x;
		Y = temp.y;
		Z = temp.z;
	    }

	    ///
	    /// \brief		Returns the contents of the string as a human readable
	    ///				string
	    /// \return
	    /// \since
	    /// \author		Dean
	    ///
	    std::string ToString() const
	    {
		std::stringstream result;

		for (size_t z = 0; z < m_zDim; ++z)
		{
		    result << "Slice Z = " << z << ".\n";

		    for (size_t y = 0; y < m_yDim; ++y)
		    {
			for (size_t x = 0; x < m_xDim; ++x)
			{
			    result << std::setw(12) << std::setprecision(6);
			    result << m_data[index3toIndex1(x, y, z)] << "\t";
			}
			result << "\n";
		    }
		}
		return result.str();
	    }

	    ///
	    /// \brief		Shifts the entire lattice across in the X direction
	    /// \return		unsigned long: the number of units traslated from zero.
	    ///	\author		Dean
	    /// \since		30-01-2015
	    ///
	    unsigned long TranslatePosX()
	    {
		//	Copy the Y and Z values of the upper limit on the X axis
		Vector2<T> temp = SlicePlaneYZ(m_xDim-1);

		//	Shift columns across
		for (long x = m_xDim-2; x >= 0; --x)
		{
		    for (unsigned long y = 0; y < m_yDim; ++y)
		    {
			for (unsigned long z = 0; z < m_zDim; ++z)
			{
			    m_data[index3toIndex1(x + 1, y, z)]
				    = m_data[index3toIndex1(x, y, z)];
			}
		    }
		}

		//	Copy temp storage to the 0th column
		for (unsigned long y = 0; y < m_yDim; ++y)
		{
		    for (unsigned long z = 0; z < m_zDim; ++z)
		    {
			m_data[index3toIndex1(0, y, z)] = temp.Get(y,z);
		    }
		}

		//	Update the shift count
		if (m_translationX == m_xDim - 1)
		{
		    m_translationX = 0;
		}
		else
		{
		    ++m_translationX;
		}

		return m_translationX;
	    }

	    ///
	    /// \brief		Shifts the entire lattice across in the X direction
	    /// \return		unsigned long: the number of units traslated from zero.
	    ///	\author		Dean
	    /// \since		30-01-2015
	    ///
	    unsigned long TranslateNegX()
	    {
		//	Copy the Y and Z values of the lower limit on the X axis
		Vector2<T> temp = SlicePlaneYZ(0);

		//	Shift columns across
		for (unsigned long x = 1; x < m_xDim; ++x)
		{
		    for (unsigned long y = 0; y < m_yDim; ++y)
		    {
			for (unsigned long z = 0; z < m_zDim; ++z)
			{
			    m_data[index3toIndex1(x - 1, y, z)]
				    = m_data[index3toIndex1(x, y, z)];
			}
		    }
		}

		//	Copy temp storage to the last column
		for (unsigned long y = 0; y < m_yDim; ++y)
		{
		    for (unsigned long z = 0; z < m_zDim; ++z)
		    {
			m_data[index3toIndex1(m_xDim-1, y, z)] = temp.Get(y, z);
		    }
		}

		//	Update the shift count
		if (m_translationX == 0)
		{
		    m_translationX = m_xDim - 1;
		}
		else
		{
		    --m_translationX;
		}

		return m_translationX;
	    }

	    ///
	    /// \brief		Shifts the entire lattice across in the Y direction
	    /// \return		unsigned long: the number of units traslated from zero.
	    ///	\author		Dean
	    /// \since		30-01-2015
	    ///
	    unsigned long TranslatePosY()
	    {
		//	Copy the X and Z values of the upper limit on the Y axis
		Vector2<T> temp = SlicePlaneXZ(m_yDim-1);

		//	Shift columns across
		for (unsigned long x = 0; x < m_xDim; ++x)
		{
		    for (long y = m_yDim-2; y >= 0; --y)
		    {
			for (unsigned long z = 0; z < m_zDim; ++z)
			{
			    m_data[index3toIndex1(x, y + 1, z)]
				    = m_data[index3toIndex1(x, y, z)];
			}
		    }
		}

		//	Copy temp storage to the 0th column
		for (unsigned long x = 0; x < m_xDim; ++x)
		{
		    for (unsigned long z = 0; z < m_zDim; ++z)
		    {
			m_data[index3toIndex1(x, 0, z)] = temp.Get(x, z);
		    }
		}

		//	Update the shift count
		if (m_translationY == m_yDim - 1)
		{
		    m_translationY = 0;
		}
		else
		{
		    ++m_translationY;
		}

		return m_translationY;
	    }

	    ///
	    /// \brief		Shifts the entire lattice across in the Y direction
	    /// \return		unsigned long: the number of units traslated from zero.
	    ///	\author		Dean
	    /// \since		30-01-2015
	    ///
	    unsigned long TranslateNegY()
	    {
		//	Copy the X and Z values of the lover limit on the Y axis
		Vector2<T> temp = SlicePlaneXZ(0);

		//	Shift columns across
		for (unsigned long x = 0; x < m_xDim; ++x)
		{
		    for (unsigned long y = 1; y < m_yDim; ++y)
		    {
			for (unsigned long z = 0; z < m_zDim; ++z)
			{
			    m_data[index3toIndex1(x, y - 1, z)]
				    = m_data[index3toIndex1(x, y, z)];
			}
		    }
		}

		//	Copy temp storage to the last column
		for (unsigned long x = 0; x < m_xDim; ++x)
		{
		    for (unsigned long z = 0; z < m_zDim; ++z)
		    {
			m_data[index3toIndex1(x, m_yDim-1, z)] = temp.Get(x, z);
		    }
		}

		//	Update the shift count
		if (m_translationY == 0)
		{
		    m_translationY = m_yDim - 1;
		}
		else
		{
		    --m_translationY;
		}

		return m_translationY;
	    }

	    ///
	    /// \brief		Shifts the entire lattice across in the Z direction
	    /// \return		unsigned long: the number of units traslated from zero.
	    /// \since		30-01-2015
	    /// \author		Dean
	    ///	\author		Dean: amend to take advantage of 2D slicing functions
	    ///				[23-09-2015]
	    unsigned long TranslatePosZ()
	    {
		//	Copy the X and Y values of the upper limit on the Z axis
		Vector2<T> temp = SlicePlaneXY(m_zDim-1);

		//	Shift columns across
		for (unsigned long x = 0; x < m_xDim; ++x)
		{
		    for (unsigned long y = 0; y < m_yDim; ++y)
		    {
			for (long z = m_zDim-2; z >= 0; --z)
			{
			    m_data[index3toIndex1(x, y, z + 1)]
				    = m_data[index3toIndex1(x, y, z)];
			}
		    }
		}

		//	Copy temp storage to the 0th column
		for (unsigned long x = 0; x < m_xDim; ++x)
		{
		    for (unsigned long y = 0; y < m_yDim; ++y)
		    {
			m_data[index3toIndex1(x, y, 0)] = temp.Get(x,y);
		    }
		}

		//	Update the shift count
		if (m_translationZ == m_zDim - 1)
		{
		    m_translationZ = 0;
		}
		else
		{
		    ++m_translationZ;
		}
		return m_translationZ;
	    }

	    ///
	    /// \brief		Shifts the entire lattice across in the Z direction
	    /// \return		unsigned long: the number of units traslated from zero.
	    /// \since		30-01-2015
	    ///	\author		Dean
	    ///	\author		Dean: amend to take advantage of 2D slicing functions
	    ///				[23-09-2015]
	    ///
	    unsigned long TranslateNegZ()
	    {
		//	Copy the X and Y values of the lower limit on the Z axis
		Vector2<T> temp = SlicePlaneXY(0);

		//	Shift columns across
		for (unsigned long x = 0; x < m_xDim; ++x)
		{
		    for (unsigned long y = 0; y < m_yDim; ++y)
		    {
			for (unsigned long z = 1; z < m_zDim; ++z)
			{
			    m_data[index3toIndex1(x, y, z - 1)]
				    = m_data[index3toIndex1(x, y, z)];
			}
		    }
		}

		//	Copy temp storage to the last column
		for (unsigned long x = 0; x < m_xDim; ++x)
		{
		    for (unsigned long y = 0; y < m_yDim; ++y)
		    {
			m_data[index3toIndex1(x, y, m_zDim-1)] = temp.Get(x,y);
		    }
		}

		//	Update the shift count
		if (m_translationZ == 0)
		{
		    m_translationZ = m_zDim - 1;
		}
		else
		{
		    --m_translationZ;
		}
		return m_translationZ;
	    }

	    ///
	    /// \brief SetTranslation
	    /// \param x
	    /// \param y
	    /// \param z
	    /// \since
	    /// \author		Dean
	    /// \author		Dean: use asserts instead of exceptions for bounds
	    ///				checking [22-09-2015]
	    ///
	    void SetTranslation(const unsigned long x,
				const unsigned long y,
				const unsigned long z)
	    {
		assert (x < m_xDim);
		assert (y < m_yDim);
		assert (z < m_zDim);

		//	Set X translation
		if (x > m_translationX)
		{
		    while (m_translationX != x)
			TranslateNegX();
		}
		else if (x < m_translationX)
		{
		    while (m_translationX != x)
			TranslatePosX();
		}

		//	Set Y translation
		if (y > m_translationY)
		{
		    while (m_translationY != y)
			TranslateNegY();
		}
		else if (y < m_translationY)
		{
		    while (m_translationY != y)
			TranslatePosY();
		}

		//	Set Z translation
		if (z > m_translationZ)
		{
		    while (m_translationZ != z)
			TranslateNegZ();
		}
		else if (z < m_translationZ)
		{
		    while (m_translationZ != z)
			TranslatePosZ();
		}
	    }

	    ///
	    /// \brief		Slices the data in the XY plane at the specifed z value
	    /// \param		z
	    /// \return
	    /// \since		23-09-2015
	    /// \author		Dean
	    ///
	    Vector2<T> SlicePlaneXY(const size_t& z) const
	    {
		assert (z < m_zDim);

		std::vector<T> temp(m_xDim * m_yDim);

		//	Copy the X and Y values in the z row
		size_t i = 0;

		//	Notice that 2D array expects the data to loop over the x
		//	domain as the inner loop
		for (size_t y = 0; y < m_yDim; ++y)
		{
		    for (size_t x = 0; x < m_xDim; ++x)
		    {
			temp[i] = m_data[index3toIndex1(x, y, z)];
			++i;
		    }
		}
		return Vector2<T>(m_xDim, m_yDim, temp);
	    }

	    ///
	    /// \brief		Slices the data in the XZ plane at the specifed y value
	    /// \param		y
	    /// \return
	    /// \since		23-09-2015
	    /// \author		Dean
	    ///
	    Vector2<T> SlicePlaneXZ(const size_t& y) const
	    {
		assert (y < m_yDim);

		std::vector<T> temp(m_xDim * m_zDim);

		//	Copy the X and Y values in the z row
		size_t i = 0;

		//	Notice that 2D array expects the data to loop over the x
		//	domain as the inner loop
		for (size_t z = 0; z < m_zDim; ++z)
		{
		    for (size_t x = 0; x < m_xDim; ++x)
		    {
			temp[i] = m_data[index3toIndex1(x, y, z)];
			++i;
		    }
		}
		return Vector2<T>(m_xDim, m_zDim, temp);
	    }

	    ///
	    /// \brief		Slices the data in the YZ plane at the specifed x value
	    /// \param		x
	    /// \return
	    /// \since		23-09-2015
	    /// \author		Dean
	    ///
	    Vector2<T> SlicePlaneYZ(const size_t& x) const
	    {
		assert (x < m_xDim);

		std::vector<T> temp(m_yDim * m_zDim);

		//	Copy the X and Y values in the z row
		size_t i = 0;

		//	Notice that 2D array expects the data to loop over the y
		//	domain as the inner loop
		for (size_t z = 0; z < m_zDim; ++z)
		{
		    for (size_t y = 0; y < m_yDim; ++y)
		    {
			temp[i] = m_data[index3toIndex1(x, y, z)];
			++i;
		    }
		}
		return Vector2<T>(m_yDim, m_zDim, temp);
	    }
    };
}

#endif //   VECTOR_3_H
