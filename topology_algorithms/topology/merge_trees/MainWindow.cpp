#include "MainWindow.h"
#include "ui_MainWindow.h"

#include "HeightField3D.h"
#include "join_tree.h"
#include "split_tree.h"

#include <iostream>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_actionTest_triggered()
{
	auto filename = QFileDialog::getOpenFileName(
				this,
				"Open scalar heightfield",
				QCoreApplication::applicationDirPath(),
				"ascii format 3D heighfield (*.txt)");
	if (filename.isEmpty()) return;

	QFileInfo fileInfo(filename);
	if (fileInfo.exists(filename))
	{
		HeightField3D data;

		if (!data.LoadFromASCII(filename.toStdString().c_str()))
		{
			ui->textEdit->setText("Unable to parse file");
			return;
		}

		JoinTree mergeTree;
		mergeTree.createTree(&data);
		mergeTree.extractTree(&data, false);
		mergeTree.removeRegularNodes();

		for (auto& x : mergeTree.m_arcs)
		{
			std::cout << *x << std::endl;
		}

		//auto dot = mergeTree.ToDotString(&data);
		//std::cout << dot  << std::endl;

		//ui->textEdit->setText(QString::fromStdString(dot));
	}
}

void MainWindow::on_actionSplit_Tree_triggered()
{
	auto filename = QFileDialog::getOpenFileName(
				this,
				"Open scalar heightfield",
				QCoreApplication::applicationDirPath(),
				"ascii format 3D heighfield (*.txt)");
	if (filename.isEmpty()) return;

	QFileInfo fileInfo(filename);
	if (fileInfo.exists(filename))
	{
		HeightField3D data;

		if (!data.LoadFromASCII(filename.toStdString().c_str()))
		{
			ui->textEdit->setText("Unable to parse file");
			return;
		}

		SplitTree mergeTree;
		mergeTree.createTree(&data);
		mergeTree.extractTree(&data, true);
		mergeTree.removeRegularNodes();

		auto dot = mergeTree.ToDotString(&data);
		std::cout << dot  << std::endl;

		ui->textEdit->setText(QString::fromStdString(dot));

		for (auto& x : mergeTree.m_arcs)
		{
			std::cout << *x << std::endl;
		}

	}
}
