#include "HeightField3D.h"
#include <regex>

#include "HeightSort.h"

#ifndef NO_TOPOLOGY
#include <Graphs/ContourTree.h>
#endif

#ifndef NO_DATA_MODEL
#include "DataModel.h"
#endif
//#include "Parallel/ThreadQueue.h"

#ifdef BASIC_EXCEPTIONS
#include <exception>
#endif


#include <limits>
#include <cmath>

#ifndef NO_TOPOLOGY
#include "NeighbourQueue/NeighbourQueue.h"
#endif

#include "Vol1.h"

#include <ios>

using namespace std;

void HeightField3D::SetDimensions(const size_type x,
								  const size_type y,
								  const size_type z)
{
	m_data.resize(x, y, z);
}

std::string HeightField3D::GenerateCacheFilename(const std::string& fieldName,
												 const std::string& variableName,
												 const size_type& variableVal)
{
	stringstream result;

	result << fieldName << "_" << variableName << "=" << variableVal << ".vol1";

	return result.str();
}
///
/// \brief		Creates a filename that can be used to lookup and store the
///				volume in the cache.  The filename does *not* contain the path.
///				example: "spacelike plaquette_t=20.vol1"
/// \return
/// \since		19-11-2015
/// \autohr		Dean
///
std::string HeightField3D::GetCacheFilename() const
{
	return GenerateCacheFilename(m_fieldName,
								 m_fixedVariableName,
								 m_fixedVariable);
}

///
/// \brief HeightField3D::SaveToFile
/// \param filename
/// \return
/// \since		Dean
/// \author		19-11-2015
///
bool HeightField3D::SaveToFile(const std::string& filename) const
{
	if (filename.find("vol1") == string::npos)
	{
		cerr << "Filetype unknown!" << endl;
		return false;
	}

	Vol1Writer<value_type> fw(m_ensembleName,
							  m_fieldName,
							  m_configurationName,
							  m_coolingSlice,
							  m_fixedVariable,
							  m_data);

	return fw(filename);
}

///
/// \brief		Deletes the cached slices
/// \details	If the data is changed (translated etc), the slices will be
///				invalid.  Hence, we delete them and they can be regenerated
///				when needed next.
/// \since		28-09-2015
/// \author		Dean
///
void HeightField3D::clearSliceCaches()
{
	m_xySlices.clear();
	m_xzSlices.clear();
	m_yzSlices.clear();
}

///
/// \brief		Returns the specified YZ plane of the data.  If the data has
///				not	yet been cached, it will be sliced
/// \param		x
/// \return
/// \since		28-09-2015
/// \author		Dean
///
HeightField3D::RealArray2d HeightField3D::GetPlaneYZ(const size_t& x)
{
	//	Valid x index?
	assert (x <= m_data.GetDimX());

	//	We still need to compute the individual slices
	if (m_yzSlices.empty())
	{
		sliceIntoPlanesYZ(true);
	}

	//	Check that the number of computed slices matches the requested slice
	assert (x < m_yzSlices.size());

	return m_yzSlices[x];
}

///
/// \brief HeightField3D::SlicePlaneYZ
/// \param generateGhostCells
/// \since		28-09-2015
/// \author		Dean
///
void HeightField3D::sliceIntoPlanesYZ(const bool &)
{
	m_yzSlices.clear();
	m_yzSlices.reserve(m_data.GetDimX());

	for (auto x = 0ul; x < m_data.GetDimX(); ++x)
	{
		m_yzSlices.push_back(m_data.SlicePlaneYZ(x));
	}
}

///
/// \brief		Returns the specified XZ plane of the data.  If the data has
///				not	yet been cached, it will be sliced
/// \param		y
/// \return
/// \since		28-09-2015
/// \author		Dean
///
HeightField3D::RealArray2d HeightField3D::GetPlaneXZ(const size_t& y)
{
	//	Valid y index?
	assert (y <= m_data.GetDimY());

	//	We still need to compute the individual slices
	if (m_xzSlices.empty())
	{
		sliceIntoPlanesXZ(true);
	}

	//	Check that the number of computed slices matches the requested slice
	assert (y < m_xzSlices.size());

	return m_xzSlices[y];
}

///
/// \brief HeightField3D::SlicePlaneXZ
/// \param generateGhostCells
/// \since		28-09-2015
/// \author		Dean
///
void HeightField3D::sliceIntoPlanesXZ(const bool &)
{
	m_xzSlices.clear();
	m_xzSlices.reserve(m_data.GetDimY());

	for (auto y = 0ul; y < m_data.GetDimY(); ++y)
	{
		m_xzSlices.push_back(m_data.SlicePlaneXZ(y));
	}
}

///
/// \brief		Returns the specified XY plane of the data.  If the data has
///				not	yet been cached, it will be sliced
/// \param		z
/// \return
/// \since		28-09-2015
/// \author		Dean
///
HeightField3D::RealArray2d HeightField3D::GetPlaneXY(const size_t& z)
{
	//	Valid z index?
	assert (z <= m_data.GetDimZ());

	//	We still need to compute the individual slices
	if (m_yzSlices.empty())
	{
		sliceIntoPlanesXY(true);
	}

	//	Check that the number of computed slices matches the requested slice
	assert (z < m_xySlices.size());

	return m_xySlices[z];
}

///
/// \brief HeightField3D::SlicePlaneXY
/// \param generateGhostCells
/// \since		28-09-2015
/// \author		Dean
///
void HeightField3D::sliceIntoPlanesXY(const bool &)
{
	m_xySlices.clear();
	m_xySlices.reserve(m_data.GetDimZ());

	for (auto z = 0ul; z < m_data.GetDimZ(); ++z)
	{
		m_xySlices.push_back(m_data.SlicePlaneXY(z));
	}
}


HeightField3D *HeightField3D::CreateHeightField3dWithGhostVerticesOnFarSide(const HeightField3D* input)
{
	HeightField3D *result = new HeightField3D(input->GetDataModel());

	//	TODO: create a better implementation of the current generateGhostCells
	//	function

	return result;
}

///
/// \brief		Returns the file type of the given filename
/// \param		filename - the full path to the file
/// \return		an enumeration representing the file format
/// \since		30-01-2015
/// \author		Dean
/// \author		Dean: move to HeightField3d class [06-07-2015]
///	\author		Dean: use a enum class instead of integer constants [06-07-2015]
///
HeightField3D::FileType HeightField3D::parseFiletype(const char* filename) const
{
	//	suffix of file name
	string suffix;
	suffix = string(strrchr(filename, '.'));

	printf("Filetype: %s.\n", suffix.c_str());
	fflush(stdout);

	if (suffix == ".txt")
	{
		printf("Filetype is .txt\n");
		fflush(stdout);

		return FileType::TXT;
	}
	else if (suffix == ".vol")
	{
		printf("Filetype is .vol\n");
		fflush(stdout);

		return FileType::VOL;
	}
	else if (suffix == ".vol1")
	{
		printf("Filetype is .vol1 (Scalar volume 3D)\n");
		fflush(stdout);

		return FileType::VOL1;
	}
	else if (suffix == ".vol3")
	{
		printf("Filetype is .vol3 (Vector volume 3D).\n");
		fflush(stdout);

		return FileType::VOL3;
	}
	else
	{
		printf("Filetype is unknown.\n");
		fflush(stdout);

		return FileType::UNKNOWN;
	}
}

///
/// \brief CollapsibleContourTree::SaveActiveArcsToOBJ
///	\since		15-01-2015
/// \author		Dean
///
void HeightField3D::SaveSelectedArcsToOBJ(const string) const
{
	//	Note only those contours currently visible will be rendered

	//	TODO: fix me, to allow use of contours as members of superarcs
	/*
	ContourList *contourList = m_flexibleIsosurface->GetContourList();

	printf("Contour count is: %ld.\n",
		   contourList->GetContourCount());

	for (unsigned long i = 0; i < contourList->GetContourCount(); ++i)
	{
		//if (contourList->GetContour(i)->IsSelected())
		//{
			//printf("%s\n", contourList->GetContour(i).GetOBJ().c_str());

			//	Give each contour a unique filename
			unsigned long UID = contourList->GetContour(i)->GetSuperarc()->GetSuperarcIndex();
			stringstream fname;
			fname << filename;
			fname << "_" << UID << ".obj";
			printf("Filename: %s.\n", fname.str().c_str());

			ofstream file;
			file.open(fname.str());

			if (file.is_open())
			{
				file << contourList->GetContour(i)->GetOBJ().c_str();

				file.close();

				printf("File written to %s.\n", fname.str().c_str());
			}
		//}
	}
*/
	fflush(stdout);
}

DataModel* HeightField3D::GetDataModel() const
{
	return m_dataModel;
}



///
/// \brief      routine to reset the flexible isosurface to local contours
/// \param      value
/// \since      26-11-2014
/// \author     Hamish
/// \author		Dean - move to DataModel class and raise a Notification to
///				observers when called [20-01-2015]
///
void HeightField3D::ResetLocalContours(const HeightField3D::Real value)
{
	// reset the stored isovalue
	//	GetFlexibleIsosurface()->m_isoValue = value;

	// now call the routine to set the flexible isosurface to a standard isosurface
	//	GetFlexibleIsosurface()->setLocal(GetFlexibleIsosurface()->m_isoValue);

	//	GetFlexibleIsosurface()->CalculateContourList();
}

unsigned long HeightField3D::findMatchingFacePolygons()
{
	unsigned long count = 0;

	//	TODO: fix this to use cached contours
	/*
	ContourList *contourList = GetFlexibleIsosurface()->GetContourList();

	//	MIN_Z and MAX_Z
	for (unsigned long c0 = 0; c0 < contourList->GetContourCount(); ++c0)
	{
		for (unsigned long c1 = c0 + 1; c1 < contourList->GetContourCount(); ++c1)
		{
			Contour *contour0 = contourList->GetContour(c0);
			Contour *contour1 = contourList->GetContour(c1);

			//	Test Z faces
			vector<Polygon2D*> pl0 = contour0->GetCappingPolygons(Boundary::MIN_Z);
			vector<Polygon2D*> pl1 = contour1->GetCappingPolygons(Boundary::MAX_Z);

			for (unsigned long p0 = 0; p0 < pl0.size(); ++p0)
			{
				for (unsigned long p1 = 0; p1 < pl1.size(); ++p1)
				{
					Polygon2D *polygon0 = pl0[p0];
					Polygon2D *polygon1 = pl1[p1];

					if (polygon0->IsCongruentTo(*polygon1))
					{
						//	Contour1 will be automatically addded bo this call
						contour0->GetSuperarc()->AddLinkedSuperarc(contour1->GetSuperarc());

						printf("Found two matching face polygons: %s\n%s\n",
							   polygon0->ToString().c_str(),
							   polygon1->ToString().c_str());
						fflush(stdout);
						++count;
					}
				}
			}
		}
	}

	//	MAX_Z and MIN_Z
	for (unsigned long c0 = 0; c0 < contourList->GetContourCount(); ++c0)
	{
		for (unsigned long c1 = c0 + 1; c1 < contourList->GetContourCount(); ++c1)
		{
			Contour *contour0 = contourList->GetContour(c0);
			Contour *contour1 = contourList->GetContour(c1);

			//	Test Z faces
			vector<Polygon2D*> pl0 = contour0->GetCappingPolygons(Boundary::MAX_Z);
			vector<Polygon2D*> pl1 = contour1->GetCappingPolygons(Boundary::MIN_Z);

			for (unsigned long p0 = 0; p0 < pl0.size(); ++p0)
			{
				for (unsigned long p1 = 0; p1 < pl1.size(); ++p1)
				{
					Polygon2D *polygon0 = pl0[p0];
					Polygon2D *polygon1 = pl1[p1];

					if (polygon0->IsCongruentTo(*polygon1))
					{
						//	Contour1 will be automatically addded bo this call
						contour0->GetSuperarc()->AddLinkedSuperarc(contour1->GetSuperarc());

						printf("Found two matching face polygons: %s\n%s\n",
							   polygon0->ToString().c_str(),
							   polygon1->ToString().c_str());
						fflush(stdout);
						++count;
					}
				}
			}
		}
	}

	//	MIN_Y and MAX_Y
	for (unsigned long c0 = 0; c0 < contourList->GetContourCount(); ++c0)
	{
		for (unsigned long c1 = c0 + 1; c1 < contourList->GetContourCount(); ++c1)
		{
			Contour *contour0 = contourList->GetContour(c0);
			Contour *contour1 = contourList->GetContour(c1);

			//	Test Z faces
			vector<Polygon2D*> pl0 = contour0->GetCappingPolygons(Boundary::MIN_Y);
			vector<Polygon2D*> pl1 = contour1->GetCappingPolygons(Boundary::MAX_Y);

			for (unsigned long p0 = 0; p0 < pl0.size(); ++p0)
			{
				for (unsigned long p1 = 0; p1 < pl1.size(); ++p1)
				{
					Polygon2D *polygon0 = pl0[p0];
					Polygon2D *polygon1 = pl1[p1];

					if (polygon0->IsCongruentTo(*polygon1))
					{
						//	Contour1 will be automatically addded bo this call
						contour0->GetSuperarc()->AddLinkedSuperarc(contour1->GetSuperarc());

						printf("Found two matching face polygons: %s\n%s\n",
							   polygon0->ToString().c_str(),
							   polygon1->ToString().c_str());
						fflush(stdout);
						++count;
					}
				}
			}
		}
	}

	//	MAX_Y and MIN_Y
	for (unsigned long c0 = 0; c0 < contourList->GetContourCount(); ++c0)
	{
		for (unsigned long c1 = c0 + 1; c1 < contourList->GetContourCount(); ++c1)
		{
			Contour *contour0 = contourList->GetContour(c0);
			Contour *contour1 = contourList->GetContour(c1);

			//	Test Z faces
			vector<Polygon2D*> pl0 = contour0->GetCappingPolygons(Boundary::MAX_Y);
			vector<Polygon2D*> pl1 = contour1->GetCappingPolygons(Boundary::MIN_Y);

			for (unsigned long p0 = 0; p0 < pl0.size(); ++p0)
			{
				for (unsigned long p1 = 0; p1 < pl1.size(); ++p1)
				{
					Polygon2D *polygon0 = pl0[p0];
					Polygon2D *polygon1 = pl1[p1];

					if (polygon0->IsCongruentTo(*polygon1))
					{
						//	Contour1 will be automatically addded bo this call
						contour0->GetSuperarc()->AddLinkedSuperarc(contour1->GetSuperarc());

						printf("Found two matching face polygons: %s\n%s\n",
							   polygon0->ToString().c_str(),
							   polygon1->ToString().c_str());
						fflush(stdout);
						++count;
					}
				}
			}
		}
	}

	//	MIN_X and MAX_X
	for (unsigned long c0 = 0; c0 < contourList->GetContourCount(); ++c0)
	{
		for (unsigned long c1 = c0 + 1; c1 < contourList->GetContourCount(); ++c1)
		{
			Contour *contour0 = contourList->GetContour(c0);
			Contour *contour1 = contourList->GetContour(c1);

			//	Test Z faces
			vector<Polygon2D*> pl0 = contour0->GetCappingPolygons(Boundary::MIN_X);
			vector<Polygon2D*> pl1 = contour1->GetCappingPolygons(Boundary::MAX_X);

			for (unsigned long p0 = 0; p0 < pl0.size(); ++p0)
			{
				for (unsigned long p1 = 0; p1 < pl1.size(); ++p1)
				{
					Polygon2D *polygon0 = pl0[p0];
					Polygon2D *polygon1 = pl1[p1];

					if (polygon0->IsCongruentTo(*polygon1))
					{
						//	Contour1 will be automatically addded bo this call
						contour0->GetSuperarc()->AddLinkedSuperarc(contour1->GetSuperarc());

						printf("Found two matching face polygons: %s\n%s\n",
							   polygon0->ToString().c_str(),
							   polygon1->ToString().c_str());
						fflush(stdout);
						++count;
					}
				}
			}
		}
	}

	//	MAX_X and MIN_X
	for (unsigned long c0 = 0; c0 < contourList->GetContourCount(); ++c0)
	{
		for (unsigned long c1 = c0 + 1; c1 < contourList->GetContourCount(); ++c1)
		{
			Contour *contour0 = contourList->GetContour(c0);
			Contour *contour1 = contourList->GetContour(c1);

			//	Test Z faces
			vector<Polygon2D*> pl0 = contour0->GetCappingPolygons(Boundary::MAX_X);
			vector<Polygon2D*> pl1 = contour1->GetCappingPolygons(Boundary::MIN_X);

			for (unsigned long p0 = 0; p0 < pl0.size(); ++p0)
			{
				for (unsigned long p1 = 0; p1 < pl1.size(); ++p1)
				{
					Polygon2D *polygon0 = pl0[p0];
					Polygon2D *polygon1 = pl1[p1];

					if (polygon0->IsCongruentTo(*polygon1))
					{
						//	Contour1 will be automatically addded bo this call
						contour0->GetSuperarc()->AddLinkedSuperarc(contour1->GetSuperarc());

						printf("Found two matching face polygons: %s\n%s\n",
							   polygon0->ToString().c_str(),
							   polygon1->ToString().c_str());
						fflush(stdout);
						++count;
					}
				}
			}
		}
	}
	*/
	return count;
}

void HeightField3D::SetTranslation(const unsigned long x,
								   const unsigned long y,
								   const unsigned long z)
{
	try
	{
		m_data.SetTranslation(x,y,z);

		sortByHeight();

		//	We will be delaying this whilst doing the translation
		BuildContourTreeAndGenerateInitialContours();
	}
	catch (std::exception &ex)
	{
		fprintf(stderr, "Translation of (%ld, %ld, %ld) is invalid!");
		fprintf(stderr, "Exception caught: %s.\n", ex.what());
		fflush(stderr);
	}
}

///	Provides a temporary solution to creation of ghost cells
void HeightField3D::GenerateGhostCells()
{
	SetData(createHeightArrayWithGhostVerticesOnFarSide(m_data, 1));
}

///
/// \brief		Sets the heightfield to the provided data set, also
///				re-calculates the contour tree appropriately
/// \param		data
/// \author		Dean
/// \since		25-02-2015
void HeightField3D::SetData(HeightField3D::Array3f data)
{
	m_isLoaded = false;
	m_isBusy = true;

	m_data = data;
	//m_xDim = data.XDim();
	//m_yDim = data.YDim();
	//m_zDim = data.ZDim();
	//m_vertexCount = data.NElements();

	free(m_sortedHeights);
	m_sortedHeights = (Real **)malloc(sizeof(Real *) * data.GetElementCount());

	sortByHeight();

	m_isLoaded = true;

	BuildContourTreeAndGenerateInitialContours();

	m_isBusy = false;

	//	We will have invalidated the cahched slices, clear them and they will
	//	be regenerated as needed
	clearSliceCaches();
}

///
/// \brief		Takes the given heightfield data and adds ghost vertices
///				NOTE: only one layer of vertices per face is handled at present
/// \return		The modified array of heights
/// \since		27-02-2015
/// \author		Dean
///
HeightField3D::Array3f HeightField3D::createHeightArrayWithGhostVerticesOnBothSides(const HeightField3D::Array3f input) const
{
	//	Local reference to the original data dimensions
	const unsigned long DIM_X_INPUT = input.GetDimX();
	const unsigned long DIM_Y_INPUT = input.GetDimY();
	const unsigned long DIM_Z_INPUT = input.GetDimZ();

	//	Number of vertices per face that will be cloned
	const unsigned long GHOST_VERTEX_COUNT = 1;

	//	Allow extra room for a slice of vertices on each face
	unsigned long DIM_X_OUTPUT = DIM_X_INPUT + (GHOST_VERTEX_COUNT * 2);
	unsigned long DIM_Y_OUTPUT = DIM_Y_INPUT + (GHOST_VERTEX_COUNT * 2);
	unsigned long DIM_Z_OUTPUT = DIM_Z_INPUT + (GHOST_VERTEX_COUNT * 2);

	//	The index of the maximum extents in the original data
	const unsigned long FAR_X_INPUT = DIM_X_INPUT - 1;
	const unsigned long FAR_Y_INPUT = DIM_Y_INPUT - 1;
	const unsigned long FAR_Z_INPUT = DIM_Z_INPUT - 1;

	//	The index of the maximum extents in the transformed data
	const unsigned long FAR_X_OUTPUT = DIM_X_OUTPUT - 1;
	const unsigned long FAR_Y_OUTPUT = DIM_Y_OUTPUT - 1;
	const unsigned long FAR_Z_OUTPUT = DIM_Z_OUTPUT - 1;

	Array3f output(DIM_X_OUTPUT, DIM_Y_OUTPUT, DIM_Z_OUTPUT);

	//	Copy the original data into the centre
	for (unsigned long z = 0; z < DIM_Z_INPUT; ++z)
	{
		for (unsigned long y = 0; y < DIM_Y_INPUT; ++y)
		{
			for (unsigned long x = 0; x < DIM_X_INPUT; ++x)
			{
				output(x + 1, y + 1, z + 1) = input(x, y, z);
			}
		}
	}

	//	Clone the vertices on the outer YZ planes (swapping locations)
	for (unsigned long z = 0; z < DIM_Z_INPUT; ++z)
	{
		for (unsigned long y = 0; y < DIM_Y_INPUT; ++y)
		{

			output(0, y + 1, z + 1) = input(FAR_X_INPUT, y, z);
			output(FAR_X_OUTPUT, y + 1, z + 1) = input(0, y, z);
		}
	}

	//	Clone the vertices on the outer XZ planes (swapping locations)
	for (unsigned long z = 0; z < DIM_Z_INPUT; ++z)
	{
		for (unsigned long x = 0; x < DIM_X_INPUT; ++x)
		{
			output(x + 1, 0, z + 1) = input(x, FAR_Y_INPUT, z);
			output(x + 1, FAR_Y_OUTPUT, z + 1) = input(x, 0, z);
		}
	}


	//	Clone the vertices on the outer XY planes (swapping locations)
	for (unsigned long y = 0; y < DIM_Y_INPUT; ++y)
	{
		for (unsigned long x = 0; x < DIM_X_INPUT; ++x)
		{
			output(x + 1, y + 1, 0) = input(x, y, FAR_Z_INPUT);
			output(x + 1, y + 1, FAR_Z_OUTPUT) = input(x, y, 0);
		}
	}

	return output;
}

///
/// \brief		Handles periodic boundaries by cloning data on the near axis
///				to the far axis
/// \param		input: a 3D array of height values
/// \param		verticesToClone: the depth of vertices to copy (1 by default)
/// \return		a 3D array with cloned values appended to original height
///				values
/// \since		?
/// \author		Dean
/// \author		Dean: correct bug when checking dimensionality of incoming
///				data [27-05-2015].
///				Dean: correct shamferred apperance on far edges of data by
///				re-cloning data on far edges (XY, YZ, XZ).  Also, re-clone data
///				in the far (XYZ) corner [27-05-2015].
///
HeightField3D::Array3f HeightField3D::createHeightArrayWithGhostVerticesOnFarSide(const HeightField3D::Array3f input, const unsigned long verticesToClone)
{
	if ((verticesToClone > input.GetDimX()) ||
		(verticesToClone >  input.GetDimY()) ||
		(verticesToClone >  input.GetDimZ()))
	{
		fprintf(stderr, "Invalid dimensionality requested for cloned data!\n");
		fflush(stderr);

		return input;
	}
	else
	{
		//	Local reference to the original data dimensions
		const unsigned long DIM_X_INPUT = input.GetDimX();
		const unsigned long DIM_Y_INPUT = input.GetDimY();
		const unsigned long DIM_Z_INPUT = input.GetDimZ();

		//	Allow extra room for a slice of vertices on each face
		unsigned long DIM_X_OUTPUT = DIM_X_INPUT + verticesToClone;
		unsigned long DIM_Y_OUTPUT = DIM_Y_INPUT + verticesToClone;
		unsigned long DIM_Z_OUTPUT = DIM_Z_INPUT + verticesToClone;

		//	The index of the maximum extents in the original data is where we will
		//	begin to output cloned vertices
		const unsigned long FAR_X_OUTPUT_BEGIN = DIM_X_INPUT;
		const unsigned long FAR_Y_OUTPUT_BEGIN = DIM_Y_INPUT;
		const unsigned long FAR_Z_OUTPUT_BEGIN = DIM_Z_INPUT;

		Array3f output(DIM_X_OUTPUT, DIM_Y_OUTPUT, DIM_Z_OUTPUT);

		//	Clone the original data
		for (unsigned long z = 0; z < DIM_Z_INPUT; ++z)
		{
			for (unsigned long y = 0; y < DIM_Y_INPUT; ++y)
			{
				for (unsigned long x = 0; x < DIM_X_INPUT; ++x)
				{
					output(x, y, z) = input(x, y, z);
				}
			}
		}

		//	Clone the vertices on the min YZ planes (ie 'c' represents a plane of
		//	vertices to be cloned (if cloning multiple vertices etc)
		for (unsigned long c = 0; c < verticesToClone; ++c)
		{
			for (unsigned long z = 0; z < DIM_Z_INPUT; ++z)
			{
				for (unsigned long y = 0; y < DIM_Y_INPUT; ++y)
				{
					//	Only copy the vertices with an x coordinate of (0 + c) to
					//	the	opposing border
					output(FAR_X_OUTPUT_BEGIN + c, y, z) = input(c, y, z);
				}
			}
		}

		//	Clone the vertices on the min XZ planes (ie 'c' represents a plane of
		//	vertices to be cloned (if cloning multiple vertices etc)
		for (unsigned long c = 0; c < verticesToClone; ++c)
		{
			for (unsigned long z = 0; z < DIM_Z_INPUT; ++z)
			{
				for (unsigned long x = 0; x < DIM_X_INPUT; ++x)
				{
					//	Only copy the vertices with an y coordinate of (0 + c) to
					//	the	opposing border
					output(x, FAR_Y_OUTPUT_BEGIN + c, z) = input(x, c, z);
				}
			}
		}

		//	Clone the vertices on the min XY planes (ie 'c' represents a plane of
		//	vertices to be cloned (if cloning multiple vertices etc)
		for (unsigned long c = 0; c < verticesToClone; ++c)
		{
			for (unsigned long y = 0; y < DIM_Y_INPUT; ++y)
			{
				for (unsigned long x = 0; x < DIM_X_INPUT; ++x)
				{
					//	Only copy the vertices with an z coordinate of (0 + c) to
					//	the	opposing border
					output(x, y, FAR_Z_OUTPUT_BEGIN + c) = input(x, y, c);
				}
			}
		}

		//	Next clone another copy of the data to fill the edges - otherwise
		//	the data will look chamfered on the far edges
		for (unsigned long c = 0; c < verticesToClone; ++c)
		{
			for (unsigned long z = 0; z < DIM_Z_INPUT; ++z)
			{
				//	Only copy the vertices with an x and y coordinate of (0 + c)
				//	to	the	opposing border
				output(FAR_X_OUTPUT_BEGIN + c, FAR_Y_OUTPUT_BEGIN + c, z)
						= input(c, c, z);
			}
		}

		//	Next clone another copy of the data to fill the edges - otherwise
		//	the data will look chamfered on the far edges
		for (unsigned long c = 0; c < verticesToClone; ++c)
		{
			for (unsigned long y = 0; y < DIM_Y_INPUT; ++y)
			{
				//	Only copy the vertices with an x and z coordinate of (0 + c)
				//	to	the	opposing border
				output(FAR_X_OUTPUT_BEGIN + c, y, FAR_Z_OUTPUT_BEGIN + c)
						= input(c, y, c);
			}
		}

		//	Next clone another copy of the data to fill the edges - otherwise
		//	the data will look chamfered on the far edges
		for (unsigned long c = 0; c < verticesToClone; ++c)
		{
			for (unsigned long x = 0; x < DIM_X_INPUT; ++x)
			{
				//	Only copy the vertices with an y and z coordinate of (0 + c)
				//	to	the	opposing border
				output(x, FAR_Y_OUTPUT_BEGIN + c, FAR_Z_OUTPUT_BEGIN + c)
						= input(x, c, c);
			}
		}

		//	Finally clone another copy of the data to fill the far corner of
		//	the new 3D array
		for (unsigned long c = 0; c < verticesToClone; ++c)
		{
			output(FAR_X_OUTPUT_BEGIN + c,
				   FAR_Y_OUTPUT_BEGIN + c,
				   FAR_Z_OUTPUT_BEGIN + c) = input(c, c, c);
		}

		return output;
	}
}

ContourTree *HeightField3D::GetContourTree()
{
	return m_contourTree;
}

bool HeightField3D::BuildContourTreeAndGenerateInitialContours()
{
#define IN_PARALLEL

	m_isBusy = true;

	//	If the data was loaded, next we need to generate the
	//	split, join and contour tree			s
	//if (m_heightfield->contourTree != nullptr)
	//	delete m_heightfield->contourTree;
#ifndef NO_TOPOLOGY
	m_contourTree = new ContourTree(this);
#endif
	//	Finally construct our Flexible isosurface object
	//if (m_flexibleIsosurface != nullptr)
	//	delete m_flexibleIsosurface;
	//	m_flexibleIsosurface = new FlexibleIsosurface(this);

#ifdef GENERATE_INITAL_CONTOURS
	for (unsigned long i = 0; i < m_contourTree->GetSuperarcCount(); ++i)
	{
		Superarc* superArc = m_contourTree->GetSuperarc(i);

		//	Only work with the valid arcs (some of the superarcs in the master
		//	list could have merged whilst collapsing epsilon edges).
		if (superArc->IsValid())
		{
			superArc->RequestContourAtMidpoint(true);
		}
	}
#ifdef IN_PARALLEL
	//	Execute in parallel - but block until all have completed
	ThreadQueue::GetInstance().ParallelExecute(false);
#else
	//	Execute jobs in serial
	ThreadQueue::GetInstance().SerialExecute();
#endif	//	IN_PARALLEL
#endif	//	GENERATE_INITAL_CONTOURS

	printf("SampleSum: %12.1f\n", GetSampleSum());
	fflush(stdout);

	//	CalculateContourList();
	m_isBusy = false;

	//	Tree was sucessfully created
	return true;
}

HeightField3D::Real HeightField3D::GetSampleSum() const
{
	return m_sampleSum;
}

bool HeightField3D::IsLoaded() const
{
	return m_isLoaded;
}

unsigned long HeightField3D::LargestDim() const
{
	return m_data.GetLargestDim();
}

unsigned long HeightField3D::GetVertexCount() const
{
	return m_data.GetElementCount();
}

HeightField3D::Real HeightField3D::GetMaxHeight() const
{
	return m_maxHeight;
}

HeightField3D::Real HeightField3D::GetMinHeight() const
{
	return m_minHeight;
}

void HeightField3D::SetHeightAt(const size_type x,
								const size_type y,
								const size_type z,
								const value_type& value)
{
	m_data.at(x,y,z) = value;
}

HeightField3D::value_type HeightField3D::GetHeightAt(const unsigned long x,
													 const unsigned long y,
													 const unsigned long z) const
{
	return m_data(x,y,z);
}

HeightField3D::value_type &HeightField3D::GetHeightAt(const unsigned long x,
													  const unsigned long y,
													  const unsigned long z)
{
	return m_data(x,y,z);
}

///
/// \brief		Returns the x, y and z index in a struct
/// \param		element
/// \return
/// \since		23-09-2015
/// \author		Dean
///
HeightField3D::Index3d HeightField3D::ComputeIndex(const Real *element) const
{
	size_t x, y, z;
	m_data.ComputeIndex(element, x, y, z);

	return {x,y,z};
}

void HeightField3D::ComputeIndex(const Real *element,
								 size_t &x,
								 size_t &y,
								 size_t &z) const
{
	//	Hack: depricated function (use above function instead)
	m_data.ComputeIndex(element, x,y,z);
}

///
/// \brief		Returns a pointer to the height data using the list
///				of sorted height held internally
/// \param		index: the index of the value to return in the sorted
///				list
/// \return		A pointer to the original height data
/// \author		Dean
/// \since		19-02-2015
///
HeightField3D::Real *HeightField3D::GetSortedHeight(const unsigned long index) const
{
	if (index < m_data.GetElementCount())
	{
		return m_sortedHeights[index];
	}
	else
	{
#ifdef BASIC_EXCEPTIONS
		throw std::invalid_argument("Invalid undex");
#else
		throw InvalidIndexException(index, __func__, __FILE__, __LINE__);
#endif
	}
}


///
/// \brief		Returns a string representation of the lattice values
/// \return		string: The values at each point, sliced into z planes.
///	\author		Dean
/// \since		30-01-2015
///
string HeightField3D::ToString() const
{
	stringstream result;

	result << "Untransformed data:\n\n";
	result << m_data.ToString();

	return result.str();
}

///
/// \brief		Shifts the entire lattice across in the X direction
/// \return		unsigned long: the number of units traslated from zero.
///	\author		Dean
/// \since		30-01-2015
///
unsigned long HeightField3D::TranslatePosX(const bool recalculateContourTree)
{
	unsigned long result = m_data.TranslatePosX();

	//	Regenerate the sorted vertices as the positions will be invalid
	sortByHeight();

	//	The previous isosurfaces and contour tree will be invalid and
	//	require recalculation
	if (recalculateContourTree)
		BuildContourTreeAndGenerateInitialContours();

	return result;
}

///
/// \brief		Shifts the entire lattice across in the X direction
/// \return		unsigned long: the number of units traslated from zero.
///	\author		Dean
/// \since		30-01-2015
///
unsigned long HeightField3D::TranslateNegX(const bool recalculateContourTree)
{
	unsigned long result = m_data.TranslateNegX();

	//	Regenerate the sorted vertices as the positions will be invalid
	sortByHeight();

	//	The previous isosurfaces and contour tree will be invalid and
	//	require recalculation
	if (recalculateContourTree)
		BuildContourTreeAndGenerateInitialContours();

	return result;
}

///
/// \brief		Shifts the entire lattice across in the Y direction
/// \return		unsigned long: the number of units traslated from zero.
///	\author		Dean
/// \since		30-01-2015
///
unsigned long HeightField3D::TranslatePosY(const bool recalculateContourTree)
{
	unsigned long result = m_data.TranslatePosY();

	//	Regenerate the sorted vertices as the positions will be invalid
	sortByHeight();

	//	The previous isosurfaces and contour tree will be invalid and
	//	require recalculation
	if (recalculateContourTree)
		BuildContourTreeAndGenerateInitialContours();

	return result;
}

///
/// \brief		Shifts the entire lattice across in the Y direction
/// \return		unsigned long: the number of units traslated from zero.
///	\author		Dean
/// \since		30-01-2015
///
unsigned long HeightField3D::TranslateNegY(const bool recalculateContourTree)
{
	unsigned long result = m_data.TranslateNegY();

	//	Regenerate the sorted vertices as the positions will be invalid
	sortByHeight();

	//	The previous isosurfaces and contour tree will be invalid and
	//	require recalculation
	if (recalculateContourTree)
		BuildContourTreeAndGenerateInitialContours();

	return result;
}

///
/// \brief		Shifts the entire lattice across in the Z direction
/// \return		unsigned long: the number of units traslated from zero.
///	\author		Dean
/// \since		30-01-2015
///
unsigned long HeightField3D::TranslatePosZ(const bool recalculateContourTree)
{
	unsigned long result = m_data.TranslatePosZ();

	//	Regenerate the sorted vertices as the positions will be invalid
	sortByHeight();

	//	The previous isosurfaces and contour tree will be invalid and
	//	require recalculation
	if (recalculateContourTree)
		BuildContourTreeAndGenerateInitialContours();

	return result;
}

///
/// \brief		Shifts the entire lattice across in the Z direction
/// \return		unsigned long: the number of units traslated from zero.
///	\author		Dean
/// \since		30-01-2015
///
unsigned long HeightField3D::TranslateNegZ(const bool recalculateContourTree)
{
	unsigned long result = m_data.TranslateNegZ();

	//	Regenerate the sorted vertices as the positions will be invalid
	sortByHeight();

	//	The previous isosurfaces and contour tree will be invalid and
	//	require recalculation
	if (recalculateContourTree)
		BuildContourTreeAndGenerateInitialContours();

	return result;
}

///
/// \brief      Constructor
/// \details	Creates the object, and automatically computes the contour tree.
///             Does the following:
///
///             A.	read in the dimensions
///             B.	set nVertices
///             C.	allocate leafQueue to a default size, and set leafQSize
///             D.	allocate height and m_sortedHeights
///             E.	read in the data, setting pointers in m_sortedHeights, and finding maxHeight and minHeight in the process
///             F.	call qsort() to sort m_sortedHeights
///             set the first & last bytes of the buffer to '\0'
///
/// \param      argc: the number of parameters passed to the program by the command line
/// \param      argv: an array of command line parameters
/// \author     Hamish
/// \since      25-11-2014
///
HeightField3D::HeightField3D(DataModel *dataModel)
{
	m_dataModel = dataModel;

	m_isLoaded = false;
	m_isBusy = false;

	//  Surfaces wrap around boundaries
	m_periodicBoundaries = true;

	m_configurationName = "Unknown configuration";
	m_fieldName = "Unknown variable";
	m_coolingSlice = 0;
	m_fixedVariable = 0;
	m_mu = 0.0f;
	m_contourTreeIsReady = false;
	m_fixedVariableName = "unknown";

	//	Start with <0,0,0> representing <0,0,0> in the actual data
	//m_translationX = 0;
	//m_transltaionY = 0;
	//m_translationZ = 0;

	m_axisLabelX = 'x';
	m_axisLabelY = 'y';
	m_axisLabelZ = 'z';

}

///
/// \brief		Loads a file of undetermined filetype and optionally computes
///				the contour tree.  Replaces old contructor.
/// \param		filename
/// \param		createContourTree
/// \return
/// \since		14-07-2016
/// \author		Dean
///
bool HeightField3D::LoadFromFile(const string& filename,
								 const bool generateGhostCells,
								 const bool createContourTree)
{
	FileType fileType = parseFiletype(filename.c_str());

	switch (fileType)
	{
		case FileType::TXT:
			m_isLoaded = LoadFromASCII(filename.c_str());
			break;
		case FileType::VOL1:
			m_isLoaded = LoadFromVol1(filename.c_str());
			break;
		default:
			cerr << "Unable to load 3D volume file: ' << filename '." << endl;
			return false;
			break;
	}

	//	Add a line of ghost cells on the far end of each axis
	//	BUG: something in here causes the mesh generator to go into an
	//	infinite loop for some configurations?
	if (generateGhostCells)	GenerateGhostCells();

	//	...
	//	Compute contour tree
	if (createContourTree)
	{
		m_contourTreeIsReady = BuildContourTreeAndGenerateInitialContours();
		return m_contourTreeIsReady;
	}
	else
	{
		return true;
	}
}

///
/// \brief		Loads a data from an array and optionally recomputes the
///				contourt tree.  Replaces old contructor.
/// \param		filename
/// \param		createContourTree
/// \return
/// \since		14-07-2016
/// \author		Dean
///
bool HeightField3D::LoadFromData(const HeightField3D::Array3f& data,
								 const bool createContourTree)
{
	m_data = data;

	unsigned long vertexCount =
			data.GetDimX() * data.GetDimY() * data.GetDimZ();

	m_sortedHeights = (Real **)malloc(sizeof(Real *) * vertexCount);
	sortByHeight();

	m_minHeight = *m_sortedHeights[0];
	m_maxHeight = *m_sortedHeights[vertexCount-1];

	//	...
	//	Compute contour tree
	if (createContourTree)
	{
		m_contourTreeIsReady = BuildContourTreeAndGenerateInitialContours();
	}

	return true;
}

///
/// \brief      Destructor
/// \details	Destroys the object, and deallocates dynamically allocated storage
/// \since      25-11-2014
/// \author     Hamish - original implementation
/// \author     Dean - most of this is now handled by ContourTree, JoinTree, SplitTree
///             and CollapsibleContourTree
///
HeightField3D::~HeightField3D()
{
	//	Hack: heights shouldn't be sorted if we are just
	//	slicing the data
#ifndef NO_TOPOLOGY
	//	release m_sortedHeights, if it exists
	if (m_sortedHeights != NULL)
		free(m_sortedHeights);
	m_sortedHeights = NULL;
#endif
}

void HeightField3D::sortByHeight()
{
#define SUPRESS_OUTPUT
	//	E.	read in the data, setting pointers in m_sortedHeights,
	//	and finding maxHeight and minHeight in the process
	//	used to walk through m_sortedHeights: start at front of m_sortedHeights
	Real **heightWalk = m_sortedHeights;

	//	Assign values to the sorting array
	for (unsigned long i = 0; i < m_data.GetDimX(); i++)
	{
		for (unsigned long j = 0; j < m_data.GetDimY(); j++)
		{
			for (unsigned long k = 0; k < m_data.GetDimZ(); k++)
			{
				//	copy a pointer into sorting array
				*heightWalk = &(m_data(i, j, k));

				//	step to next pointer in sorting array
				heightWalk++;
			}
		}
	}
#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
	printf("Sorting vertices by height.\n");
	fflush(stdout);
#endif

//#ifndef NO_TOPOLOGY
	qsort(m_sortedHeights, m_data.GetElementCount(),
		  sizeof(Real *), compareHeightVoid);
//#endif

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
	printf("%s\n", GetSortedHeightsAsString().c_str());
	fflush(stdout);
#endif
#undef SUPRESS_OUTPUT
}

bool is_numeric(const std::string &str)
{
	std::istringstream ss(str);

	double dbl;

	//	Try to read the number and consume additional whitespace
	ss >> dbl;
	ss >> std::ws;

	if (!ss.fail() && ss.eof())
	{
		return true;
	}
	else
	{
		return false;
	}
}


///
/// \brief      loads a file from ASCII text
/// \param      fileName
/// \since      25-11-2014
/// \author     Hamish
///
bool HeightField3D::LoadFromASCII(const char* fileName)
{
	m_isBusy = true;

	string buffer;
	unsigned long xDim, yDim, zDim;

	printf("Loading ASCII file: %s.\n", fileName);
	fflush(stdout);

	ifstream fin(fileName);

	if (!fin)
	{
		printf("Unable to open file %s\n", fileName);
		return false;
	}

	//	A.	copy the parameters into the object
	//	read in the dimensions
	fin >> buffer;
	if (is_numeric(buffer))
	{
		std::istringstream(buffer) >> xDim;
	}
	fin >> buffer;
	if (is_numeric(buffer))
	{
		std::istringstream(buffer) >> yDim;
	}
	fin >> buffer;
	if (is_numeric(buffer))
	{
		std::istringstream(buffer) >> zDim;
	}

	//	B.	set nVertices
	unsigned long vertexCount = xDim * yDim * zDim;

	//	C.	allocate leafQueue to a default size, and set leafQSize
	//  handled in contour tree constructor

	//	D.	allocate height, m_sortedHeights, and visitFlags
	//	call function to initialize the height array
	//	allocate the array
	//	and call template function to set up array

	printf("Expected dimensions are: %ld x %ld x %ld = %ld values.\n",
		   xDim, yDim, zDim, vertexCount);
	fflush(stdout);

	m_data = Array3f(xDim, yDim, zDim);
	m_sortedHeights = (Real **)malloc(sizeof(Real *) * vertexCount);
	// visitFlags.Construct(m_xDim, m_yDim, m_zDim);

	m_maxHeight = -numeric_limits<Real>::infinity();
	m_minHeight = numeric_limits<Real>::infinity();

	printf("Starting to load values from file.\n");
	fflush(stdout);

	//	walk through each dimension
	for (unsigned long i = 0; i < xDim; i++)
	{
		// 		printf("."); fflush(stdout);
		for (unsigned long j = 0; j < yDim; j++)
		{
			unsigned long k = 0;
			while (k < zDim)
			{
				//	temporary variable for reading in from array
				Real nextHeight;

				//	Use a temp variable to see if the next data in the
				//	file is numeric
				fin >> buffer;
				if (is_numeric(buffer))
				{
					//	read it in and store it in the array
					std::istringstream(buffer) >> nextHeight;
					m_data(i, j, k) = nextHeight;

					//	add to the running sum
					m_sampleSum += nextHeight;

					//	update maxHeight, minHeight
					if (nextHeight > m_maxHeight)
						m_maxHeight = nextHeight;
					if (nextHeight < m_minHeight)
						m_minHeight = nextHeight;

					//	We only increment if a numeric value was parsed
					++k;
				}
			}
		}
	}

	printf("Completed loading values from file.\n");
	fflush(stdout);

	sortByHeight();

	printf("Exiting ASCII file loader.\n");
	fflush(stdout);

	m_isLoaded = true;
	m_isBusy = false;

	return true;
}

///
/// \brief      LoadFromRaw
/// \param      inFile
/// \since      25-11-2014
/// \author     Hamish - original implementation
/// \author     Dean - move dimension check here too...
///
bool HeightField3D::LoadFromRaw(const char* filename,
								const unsigned int xDim,
								const unsigned int yDim,
								const unsigned int zDim,
								const unsigned int res)
{
	/*
	m_isBusy = true;

	FILE *inFile;
	m_sampleSum = 0;

	inFile = fopen(filename, "rb");
	if (!inFile)
	{
		printf("Unable to open RAW format file %s\n", filename);
		return false;
	}

	unsigned long sampleSize = res;
	if ((sampleSize < 1) || (sampleSize > 8))
	{
		printf("Illegal sample size %d\n", sampleSize);
		return false;
	}

	if ((xDim < 1) || (xDim > 2048))
	{
		printf("Illegal x dimension %ld\n", xDim);
		return false;
	}

	if ((yDim < 1) || (yDim > 2048))
	{
		printf("Illegal y dimension %ld\n", yDim);
		return false;
	}

	if ((zDim < 1) || (zDim > 2048))
	{
		printf("Illegal z dimension %ld\n", zDim);
		return false;
	}

	long index;	//	loop index
	if (sampleSize < 1)
	{
		printf("Sample sizes less than 1 don't make sense.\n");
		return false;
	}

	if (sampleSize > 2)
	{
		printf("Sample sizes greater than 2 not yet implemented.\n");
		return false;
	}

	//	B.	set nVertices
	unsigned long vertexCount = xDim * yDim * zDim;

	//	C.	allocate leafQueue to a default size, and set leafQSize
	//  handled in contour tree constructor

	//	D.	allocate height, m_sortedHeights, and visitFlags
	m_sortedHeights = (Real **)malloc(sizeof(Real *) * vertexCount);

	//	call template function to set up array
	//	visitFlags.Construct(m_xDim, m_yDim, m_zDim);

	//	E.	read in the data, setting pointers in m_sortedHeights, and finding maxHeight and minHeight in the process
	m_maxHeight = -numeric_limits<Real>::infinity();
	m_minHeight = numeric_limits<Real>::infinity();

	//	allocate the memory for the Real representation
	Real *RealData = (Real *) malloc(sizeof(Real) * vertexCount);
	unsigned char *rawData = (unsigned char *) malloc(sizeof (unsigned char) * sampleSize * vertexCount);

	//	read in the block of data
	long nSamplesRead = fread(rawData, sizeof(unsigned char), vertexCount, inFile);

	if (nSamplesRead != vertexCount)
	{
		printf("Binary read of input file failed.\n");
		return false;
	}

	//gettimeofday(&lastTime, NULL);
	//timingBuffer += sprintf(timingBuffer, "Input size (n): %1ld x %1ld x %1ld = %1ld\n", xDim, yDim, zDim, vertexCount);
	//timingBuffer += sprintf(timingBuffer, "Reading data took %8.5f seconds\n", (Real) (lastTime.tv_sec - startTime.tv_sec) + 1E-6 * (lastTime.tv_usec - startTime.tv_usec));
	//    flushTimingBuffer();

	//	set up radix sort bins
	long nBins = (sampleSize == 1) ? 256 : 65536L;

	long firstInBin[nBins];

	//	initialize to "NONE"
	for (int i = 0; i < nBins;i++)
	{
		firstInBin[i] = -1;
	}

	//	and initialize the "next" array
	long *nextInBin = (long *) malloc(vertexCount * sizeof(long));
	for (int i = 0; i < vertexCount; i++)
	{
		nextInBin[i] = -1;
	}

	if (sampleSize == 1)
	{
		// walk through data
		for (index = 0; index < vertexCount; index++)
		{
			//	push into linked list at front of sorting bin
			nextInBin[index] = firstInBin[rawData[index]];

			//	and reset the front of the bin
			firstInBin[rawData[index]] = index;

			//	copy into the Real array
			RealData[index] = (Real) rawData[index];

			//	add to the running sum
			m_sampleSum += rawData[index];
		}
	}
	else
	{
		// 2-byte data
		unsigned short *rawShort = (unsigned short * ) rawData;

		for (index = 0; index < vertexCount; index++)
		{
			//	walk through the data
			unsigned short newShort = rawShort[index];
#ifdef ENDIAN_SWAP_SHORTS
			newShort = (newShort >> 8) | ((newShort & 0x00FF) << 8);
#endif

			//	push into linked list at front of sorting bin
			nextInBin[index] = firstInBin[newShort];

			//	and reset the front of the bin
			firstInBin[newShort] = index;

			//	copy into the Real array
			RealData[index] = (Real) newShort;

			//	add to the running sum
			m_sampleSum += newShort;
		}
	}

	//	we have now loaded up the Real array, and need to setup m_sortedHeights properly
	//	our bin sort has now pushed the elements into bins IN REVERSE ORDER, so we have to
	//	flip again in reading them out

	//	which bin we are reading from
	int bin = nBins;

	//	which sample comes next
	long nextSample = -1;

	// filling in m_sortedHeights
	for (index = vertexCount-1; index >= 0; index--)
	{
		//	this bin is empty
		while (nextSample == -1)
		{
			//	grab the first item in the next bin
			nextSample = firstInBin[--bin];
		}

		//	generate & store the pointer
		m_sortedHeights[index] = RealData+nextSample;

		//	grab the next item
		nextSample = nextInBin[nextSample];
	}

	//	call function to initialize the height array
	m_data.Construct(xDim, yDim, zDim, RealData);

	//	set plus infinity in case we ever compare against it
	//	and minus infinity
	//	plus_infinity = HUGE_VAL;
	//	minus_infinity = -HUGE_VAL;

	//	set min. and max. height
	m_maxHeight = *(m_sortedHeights[vertexCount-1]);
	m_minHeight = *(m_sortedHeights[0]);

	//	PrintSortOrder();

	//	release the working memory
	free (nextInBin);
	free(rawData);

	//gettimeofday(&thisTime, NULL);
	//timingBuffer += sprintf(timingBuffer, "Sorting data took %8.5f seconds\n",
	//							(Real) (thisTime.tv_sec - lastTime.tv_sec)
	//							+ 1E-6 * (thisTime.tv_usec - lastTime.tv_usec));

	m_isLoaded = true;
	m_isBusy = false;
	*/
	return true;
}

///
/// \brief	Loads a volume from a .vol1 formatted file
/// \param	filename: the file to be loaded
/// \return	bool: true if the file was correctly loaded
///
bool HeightField3D::LoadFromVol1(const string& filename)
{
	//	Enable to remove all writing to stdout in this function
#define SUPRESS_OUTPUT
	//string configurationName;
	//string variableName;
	//unsigned long coolingCycle;
	//unsigned long timeStep;
	string buffer;
	unsigned long dimX, dimY, dimZ;
	unsigned long expectedVertexCount;
	unsigned long actualVertexCount = 0;
	float minVal;
	float maxVal;
	float totVal;
	float aveVal;

	m_isBusy = true;
	m_sampleSum = 0;

	ifstream inFile(filename);


	if (!inFile.is_open())
	{
		cerr <<  "Unable to open VOL1 format file: " << filename << "." << endl;
		return false;
	}



	//	Read our ensemble and configuration names
	std::getline(inFile, m_ensembleName);
	std::getline(inFile, m_configurationName);

	//	Next two lines should contain the coolinc cycle and time step
	inFile >> std::skipws >> m_coolingSlice >> std::ws;
	inFile >> std::skipws >> m_fixedVariable >> std::ws;

	//	Next we should have the field variable (could contain spaces)
	std::getline(inFile, m_fieldName);

	//	Now the field dimensions
	inFile >> std::skipws >> dimX >> dimY >> dimZ;
	expectedVertexCount = dimX * dimY * dimZ;

	//	Next we should have a blank line
	std::getline(inFile, buffer);

	//	Next four values are computed stats)
	inFile >> std::skipws >> minVal >> maxVal >> totVal >> aveVal;

	if (!((dimX > 0) && (dimY > 0) && (dimZ > 0)))
	{
		cerr << "Loading failed - invalid dimensionality!" << endl;
		m_isBusy = false;
		return false;
	}

	//	Set up temp storage (for heights directly from file)
	Array3f tempData(dimX, dimY, dimZ);

	//	Initialize the sorted heights
	m_maxHeight = -numeric_limits<Real>::infinity();
	m_minHeight = numeric_limits<Real>::infinity();

	//	Start loading the file
#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
	cout << "Loading scalar volume." << endl;
	cout << "Configuration name: " << m_configurationName << "." << endl;
	cout << "Variable name: " << m_variableName << "." << endl;
	cout << "Cooling cycle: " << m_coolingCycle << "." << endl;
	cout << "Time step: " << m_timeStep << "." << endl;
	cout << "Dimensions: " << dimX << ", " << dimY << ", " << dimZ << ".";
	cout << endl;
	cout << "Unknown 1: " << unknownVariable1 << ".\n";
	cout << "Unknown 2: " << unknownVariable2 << ".\n";
	cout << "Unknown 3: " << unknownVariable3 << ".\n";
#endif

	//	Defines a function for rounding the data to a specified number of
	//	decimal places, if needed
	auto roundingFunc = [&](const float input, const size_t dp) -> float
	{
		//	Factor to multply and divide by
		auto factor = pow(10.0f, static_cast<float>(dp));

		return (round(input * factor) / factor);
	};

	while ((!inFile.eof()) && (actualVertexCount < expectedVertexCount))
	{
		//	Reading line by line
		unsigned long x, y, z;
		Real value;

		//	Read the coordinates and the data
		inFile >> x >> y >> z;
		inFile >> value;

		//value = roundingFunc(value, 1);

		//	Set the value
		tempData(x,y,z) = value;

		//	add to the running sum
		m_sampleSum += value;

		//	update maxHeight, minHeight
		if (value > m_maxHeight)
			m_maxHeight = value;
		if (value < m_minHeight)
			m_minHeight = value;

		//	Increment the counter
		++actualVertexCount;
	}

	//	Add code to create periodic boundaries
	m_data = tempData;

	//	Count number of data points
	unsigned long vertexCount =
			m_data.GetDimX() * m_data.GetDimY() * m_data.GetDimZ();
	m_sortedHeights = (Real **)malloc(sizeof(Real *) * vertexCount);

	//	Sort our heights
	sortByHeight();

	m_isLoaded = true;
	m_isBusy = false;

	//	Attempt to extract slice axis...
	regex slice_regex(R"|(.*(\w)=(\d+).vol1)|");
	smatch matches;
	if (regex_match(filename, matches, slice_regex))
	{
		cout << "Slice information: " << matches[0] << endl;
		SetFixedVariableName(matches[1]);
		SetFixedVariableValue(stoi(matches[2]));
	}
	else
	{
		cerr << "Unable to extract slice data from filename." << endl;
	}

	return true;

}

///
/// \brief		Returns the heights in sorted order as a string
/// \since		05-03-2015
/// \author		Hamish: original code
/// \author		Dean: re-implement as a function returning a string
///
///
string HeightField3D::GetSortedHeightsAsString() const
{
	stringstream result;

	result << "Sorted heights:\n\n";

	for (unsigned long i = 0; i < m_data.GetElementCount(); i++)
	{
		size_t x, y, z;
		m_data.ComputeIndex(m_sortedHeights[i], x, y, z);

		result << "[" << i << "]";
		result << "\t";
		result << "<" << setw(3) << x << ", " << setw(3)
			   << y << ", " << setw(3) << z << "> ";
		result << "\t";
		result << *(m_sortedHeights[i]);
		result << "\n";
	}
	return result.str();
}

#ifndef NO_TOPOLOGY
//	routine to find seeds for edges that do not otherwise have them
Real *HeightField3D::findDescendingNeighbour(Real *theVertex,
											 HeightField3D::Array3f heightArray)
{
	unsigned long xDim = heightArray.GetDimX();
	unsigned long yDim = heightArray.GetDimY();
	unsigned long zDim = heightArray.GetDimZ();

	NeighbourQueue neighbourQueue;

	size_t x, y, z;
	ComputeIndex(theVertex, x, y, z);								//	retrieve the indices

	NeighbourFunctor18 mooreNeighbourhood(xDim, yDim, zDim);
	neighbourQueue.QueueNeighbours({x, y, z}, mooreNeighbourhood);											//	queue up its neighbours in join order

	for (int i = 0; i < neighbourQueue.GetNeighbourCount(); i++)									//	walk along queue
	{ // for i
		Index3d temp = neighbourQueue.GetNeighbourAt(i);

		Real *neighbour = &(m_data(temp.x,
								   temp.y,
								   temp.z));
		if (compareHeight(neighbour, theVertex) < 0)							//	if the neighbour is smaller than the vertex
			return neighbour;											//	it will make a valid seed
	} // for i
	printf("Major error at %s:%d: couldn't find descending neighbour.\n", __FILE__, __LINE__);
	return NULL;
}
#endif
///
/// \brief	Takes a normalized isovalue (in range 0..1) and converts it to the
///			actual corresponding height in the data
/// \param	isovalue: a value in the range 0..1
/// \return Real: the actual corresponding height in the data
/// \since	11-05-2015
/// \author	Dean
///
HeightField3D::Real HeightField3D::ConvertNormalisedIsovalueToActualHeight(const Real& isovalue) const
{
	return (m_minHeight + (isovalue * (m_maxHeight - m_minHeight)));
}

///
/// \brief	Takes an actual height value and converts it to the a corresponding
///			normalised (0..1) isovalue
/// \param  height: the actual function height
/// \return	Real: an isovalue value in the range 0..1
/// \since	11-05-2015
/// \author	Dean
///
HeightField3D::Real HeightField3D::ConvertActualHeightToNormalisedIsovalue(const Real& height) const
{
	//	Not implemented
	fprintf(stderr, "Not implemented!  In function: %s.\n", __func__);
	fflush(stderr);
	throw 20;
}
