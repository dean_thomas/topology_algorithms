///
///		Provides a central location for types used in the mesh part of the
///		program.  This allows the precision of the mesh to be switched
///		throughout with changes here.
///

#ifndef TYPEDEFINES_H
#define TYPEDEFINES_H

//#include <Geometry/Geometry.h>
//#include "Superarc.h"
//#include "Mesh/MeshTriangle.h"

enum class DirectionOfTravel
{
	POSITIVE = 0,
	NEGATIVE = 1
};

//	Decimal precision
typedef float Real;
/*

//	Primitives
//	3D
typedef Geometry::Polygon3<Real> Polygon3D;
typedef Triangle3<Real> Triangle3D;
typedef LineSegment3<Real> Edge3D;
typedef Vector3<Real> Point3D;
typedef Vector3<Real> Vector3D;

//	2D
typedef Geometry::Polygon2<Real> Polygon2D;
typedef Triangle2<Real> Triangle2D;
typedef LineSegment2<Real> Edge2D;
typedef Vector2<Real> Point2D;
typedef Vector2<Real> Vector2D;

typedef Vector3<unsigned long> LatticeSite3D;

//	Structs
struct EdgeRecord
{
	Edge3D edge;
	unsigned long count;
};

struct VertexRecord
{
	Point3D vertex;
	unsigned long count;
};

//	Lists
typedef vector<VertexRecord> VertexList;
typedef vector<EdgeRecord> EdgeRecordList;
typedef vector<Triangle3D> TriangleList;
typedef vector<Polygon2D*> PolygonList;

typedef vector<Edge2D> EdgeList2;
//typedef vector<MeshTriangle> MeshTriangleList;
*/

#endif // TYPEDEFINES_H
