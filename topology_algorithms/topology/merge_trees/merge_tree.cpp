#include "merge_tree.h"
#include "HeightField3D.h"
#include <algorithm>

//	Needed so we can access the static pointer to pos and neg infinity
constexpr decltype(MergeTree::MINUS_INFINITY) MergeTree::MINUS_INFINITY;
constexpr decltype(MergeTree::MINUS_INFINITY_PTR) MergeTree::MINUS_INFINITY_PTR;
constexpr decltype(MergeTree::PLUS_INFINITY) MergeTree::PLUS_INFINITY;
constexpr decltype(MergeTree::PLUS_INFINITY_PTR) MergeTree::PLUS_INFINITY_PTR;

using namespace std;

MergeTree::MergeTree()
{

}

MergeTree::Component_ptr MergeTree::findComponent(const vec3& pos3) const
{
	auto it = m_componentLookup.find(pos3);
	if (it != m_componentLookup.cend())
	{
		return it->second;
	}
	else
	{
		return nullptr;
	}
}

int MergeTree::heightSort(const HeightSample_ptr& a,
						  const HeightSample_ptr& b) const
{
	assert(a != nullptr);
	assert(b != nullptr);

	cout << "Height A: " << *a << " Height B: " << *b << endl;

	if (*a < *b) return -1;
	if (*a > *b) return +1;

	if (a < b) return -1;
	if (a > b) return +1;

	cerr << "Major error: two heights sorted identically." << endl;
	assert(false);
	return 0;
}

std::deque<vec3> MergeTree::queueNeighbours(HeightField3D* heightfield,
											const vec3& pos,
											const Neighbours selectionStrategy) const
{
	assert(heightfield != nullptr);

	auto sieveValid = [&](const vec3& pos)
	{
		//	Provides a predicate to filter invalid vertices
		assert(heightfield != nullptr);

		auto dim_x = heightfield->GetData().GetDimX();
		auto dim_y = heightfield->GetData().GetDimY();
		auto dim_z = heightfield->GetData().GetDimZ();

		//	Current implementation uses unsigned integers - make sure this is
		//	not broken in newer rewrites
		static_assert(is_unsigned<decltype(vec3::x)>::value,
					  "Indices should be an unsigned integer type");
		static_assert(is_unsigned<decltype(vec3::y)>::value,
					  "Indices should be an unsigned integer type");
		static_assert(is_unsigned<decltype(vec3::z)>::value,
					  "Indices should be an unsigned integer type");

		//	As we are asserting that the 3 fields are unsigned there is no
		//	point in checking if values are less than zero.
		if (!(pos.x < dim_x)) return false;
		if (!(pos.y < dim_y)) return false;
		if (!(pos.z < dim_z)) return false;

		return true;
	};

	//	Lists will received the unfiltered and filtered lists of neighbour
	//	locations.
	std::deque<vec3> unfiltered;
	std::deque<vec3> filtered;

	switch (selectionStrategy)
	{
	case Neighbours::SIX:
		//	Represents the 6 immediate neighbours in the x, y, and z
		//	directions.
		unfiltered = { { pos.x-1, pos.y, pos.z }, { pos.x+1, pos.y, pos.z },
					   { pos.x, pos.y-1, pos.z }, { pos.x, pos.y+1, pos.z },
					   { pos.x, pos.y, pos.z-1 }, { pos.x, pos.y, pos.z+1 } };
		break;
	case Neighbours::EIGHTEEN:
		//	Represents the 6 immediate neighbours in the x, y, and z,
		//	and the immediate neighbous of those.  i.e. all the vertices in a
		//	cube surrounding the vertex APART from those in the corners.
		unfiltered = { { pos.x-1, pos.y, pos.z }, { pos.x+1, pos.y, pos.z },
					   {pos.x, pos.y-1, pos.z}, {pos.x, pos.y+1, pos.z},
					   {pos.x, pos.y, pos.z-1}, {pos.x, pos.y, pos.z+1},
					   {pos.x-1, pos.y-1, pos.z}, {pos.x+1, pos.y+1, pos.z},
					   {pos.x+1, pos.y-1, pos.z}, {pos.x-1, pos.y+1, pos.z},
					   {pos.x, pos.y-1, pos.z-1}, {pos.x, pos.y+1, pos.z+1},
					   {pos.x, pos.y+1, pos.z-1}, {pos.x, pos.y-1, pos.z+1},
					   {pos.x-1, pos.y, pos.z-1}, {pos.x+1, pos.y, pos.z+1},
					   {pos.x-1, pos.y, pos.z+1}, {pos.x+1, pos.y, pos.z-1} };
		break;
	default:
		cerr << "Unknown neighbour selection strategy." << endl;
	}

	//	Now we filter out the invalid locations and return the remaining
	//	values
	copy_if(unfiltered.cbegin(), unfiltered.cend(),
			back_inserter(filtered), sieveValid);
	return filtered;
}

//cout << "Expect " << m_criticalNodes << " critical nodes.  Got "
//	 << m_nodes.size() << endl;
//assert(m_nodes.size() == m_criticalNodes);

bool MergeTree::removeRegularNodes()
{
	cout << "Reducing merge tree" << endl;
	auto doReductionStep = false;

	do
	{
		doReductionStep = false;
		for (auto i = 0ul; i < m_nodes.size(); ++i)
		{
			//	Note: we can do a for loop with iterators - as we are
			//	potentially deleting objects.  Maybe this can be made to look
			//	nicer using the erase-remove idiom?
			auto node = m_nodes[i];
			cout << node << endl;

			if ((node->getInDegree() == 1) && (node->getOutDegree() == 1))
			{
				cout << "Found regular node" << endl;

				//	Should be a redundant test, but just incase something gets
				//	broken
				assert(node->inArcs.size() == 1);
				assert(node->outArcs.size() == 1);
				auto oldInArc = node->inArcs[0];
				auto oldOutArc = node->outArcs[0];

				//	The new source will be the next node up the tree from
				//	current source
				auto newSourceNode = oldInArc->source;

				//	The new target will be the next node down the tree from
				//	the current target
				auto newTargetNode = oldOutArc->target;

				//	Create the new arc and add to the arc list
				auto newArc = new Arc(newSourceNode, newTargetNode);
				assert(newArc != nullptr);
				m_arcs.push_back(newArc);

				//	Update persistence measures for the new arc
				newArc->collapsedNodeCount = oldInArc->collapsedNodeCount
						+ oldOutArc->collapsedNodeCount + 1;
				newArc->collapsedNodeSum = oldInArc->collapsedNodeSum
						+ oldOutArc->collapsedNodeSum + node->height;

				//	Remove the current node from the tree structure, but don't
				//	delete it yet
				auto nodeIt = find(m_nodes.begin(), m_nodes.end(), node);
				assert(nodeIt != m_nodes.end());
				m_nodes.erase(nodeIt);

				//	Remove the current in and out arcs from the tree structure,
				//	but don't delete them yet
				auto inIt = find(m_arcs.begin(), m_arcs.end(), oldInArc);
				assert(inIt != m_arcs.end());
				m_arcs.erase(inIt);
				auto outIt = find(m_arcs.begin(), m_arcs.end(), oldOutArc);
				assert(outIt != m_arcs.end());
				m_arcs.erase(outIt);

				//	Now remove the oldInArc from its sources out arcs list
				auto it1 = find(newSourceNode->outArcs.begin(),
								newSourceNode->outArcs.end(),
								oldInArc);
				assert(it1 != newSourceNode->outArcs.end());
				newSourceNode->outArcs.erase(it1);

				//	And finally remove the oldOutArc from its targets in arcs
				//	list
				auto it2 = find(newTargetNode->inArcs.begin(),
								newTargetNode->inArcs.end(),
								oldOutArc);
				assert(it2 != newTargetNode->inArcs.end());
				newTargetNode->inArcs.erase(it2);

				//	Finally, we can delete the old objects
				delete node;
				delete oldInArc;
				delete oldOutArc;

				//	We will need to loop at least once more.  Break out of
				//	the for loop to start from scratch with the updated nodes.
				doReductionStep = true;
				break;
			}
		}
	}
	while (doReductionStep);

	//	This probably needs to be adjusted for the imaginary point at infinity?
	//cout << "Expected critical nodes " << m_criticalNodes
	//	 << "; actual critical nodes " << m_nodes.size() << endl;
	//assert(m_nodes.size() == m_criticalNodes);

	return true;
}

bool MergeTree::extractTree(HeightField3D* heightField, const bool reverse_arcs)
{
	std::unordered_map<vec3, Node*> temp_node_list;

	for (unsigned long x = 0; x < heightField->GetBaseDimX(); x++)
	{
		for (unsigned long y = 0; y < heightField->GetBaseDimY(); y++)
		{
			for (unsigned long z = 0; z < heightField->GetBaseDimZ(); z++)
			{
				auto it = m_arcLookup.find({x, y, z});
				if (it != m_arcLookup.cend())
				{
					if (it->second == MINUS_INFINITY_PTR)
						continue;

					if (it->second == PLUS_INFINITY_PTR)
						continue;

					size_t xFar, yFar, zFar;
					heightField->ComputeIndex(it->second, xFar, yFar, zFar);

					Node* source_node = nullptr;
					vec3 source_pos;

					Node* target_node = nullptr;
					vec3 target_pos;

					if (reverse_arcs)
					{
						source_pos = { xFar, yFar, zFar };
						target_pos = { x, y, z };

						auto source_it = temp_node_list.find(source_pos);
						if (source_it == temp_node_list.cend())
						{
							source_node = new Node(source_pos,
												   heightField->GetHeightAt(
													   xFar, yFar, zFar));
						}
						else
						{
							source_node = source_it->second;
						}

						auto target_it = temp_node_list.find(target_pos);
						if (target_it == temp_node_list.cend())
						{
							target_node = new Node(target_pos,
												   heightField->GetHeightAt(
													   x, y, z));
						}
						else
						{
							target_node = target_it->second;
						}
					}
					else
					{
						source_pos = { x, y, z };
						target_pos = { xFar, yFar, zFar };

						auto source_it = temp_node_list.find(source_pos);
						if (source_it == temp_node_list.cend())
						{
							source_node = new Node(source_pos,
												   heightField->GetHeightAt(
													   x, y, z));
						}
						else
						{
							source_node = source_it->second;
						}

						auto target_it = temp_node_list.find(target_pos);
						if (target_it == temp_node_list.cend())
						{
							target_node = new Node(target_pos,
												   heightField->GetHeightAt(
													   xFar, yFar, zFar));
						}
						else
						{
							target_node = target_it->second;
						}

					}

					assert(source_node != nullptr);
					assert(target_node != nullptr);

					auto arc = new Arc(source_node, target_node);
					assert(arc != nullptr);

					temp_node_list[source_pos] = source_node;
					temp_node_list[target_pos] = target_node;
					m_arcs.push_back(arc);
				}
			}
		}
	}

	//	Finally we can flatten the lookup for nodes to a basic array struct
	for (auto& node : temp_node_list)
	{
		m_nodes.push_back(node.second);
	}
	return true;
}

///
/// \brief		Returns the union find as a directed graph in DOT format
/// \param		heightField
/// \param		reverse_arcs: for the split tree this should be true, for the
///				join tree false.
/// \return		A string representing the entire graph in DOT format.
///
std::string MergeTree::ToDotString(HeightField3D* heightField,
								   const bool reverse_arcs) const
{
	using namespace std;

	stringstream ss;

	ss << "digraph G {\n";
	//fprintf(dotFile, "\tsize=\"6.5, 9\"\n\tratio=\"fill\"\n");

	for (unsigned long x = 0; x < heightField->GetBaseDimX(); x++)
	{
		for (unsigned long y = 0; y < heightField->GetBaseDimY(); y++)
		{
			for (unsigned long z = 0; z < heightField->GetBaseDimZ(); z++)
			{
				auto it = m_arcLookup.find({x, y, z});
				if (it != m_arcLookup.cend())
				{
					if (it->second == MINUS_INFINITY_PTR)
						continue;

					if (it->second == PLUS_INFINITY_PTR)
						continue;

					size_t xFar, yFar, zFar;
					heightField->ComputeIndex(it->second, xFar, yFar, zFar);

					if (reverse_arcs)
					{
						//  Split Tree
						ss << "\t\"(" << xFar << "," << yFar << "," << zFar;
						ss << ")\" -> \"(";
						ss << x << "," << y << "," << z;
						ss << ")\";\n";
					}
					else
					{
						//  Join Tree
						ss << "\t\"(" << x << "," << y << "," << z;
						ss << ")\" -> \"(";
						ss << xFar << "," << yFar << "," << zFar;
						ss << ")\";\n";
					}
				}
			}
		}
	}

	ss << "}\n";

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
	printf("%s\n",ss.str().c_str());
	fflush(stdout);
#endif

	return ss.str();
#undef SUPRESS_OUTPUT
}
