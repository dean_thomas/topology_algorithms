#ifndef MERGE_TREE_H
#define MERGE_TREE_H

#include <unordered_map>
#include <iostream>
#include <limits>
#include <sstream>
#include <queue>
#include "vec3.h"

#include "component.h"

class HeightField3D;

class Arc;

struct Node
{
	using Arc_ptr = Arc*;

	vec3 position;
	float height;

	std::vector<Arc_ptr> outArcs;
	std::vector<Arc_ptr> inArcs;

	size_t getInDegree() const { return inArcs.size(); }
	size_t getOutDegree() const { return outArcs.size(); }
	size_t getDegree() const { return inArcs.size() + outArcs.size(); }

	Node(const vec3& pos, const float height)
		: position { pos }, height { height }
	{ }
};

inline std::ostream& operator << (std::ostream& os, const Node& node)
{
	os << "deg = " << node.getDegree()
	   << "; in deg = " << node.getInDegree()
	   << "; out deg = " << node.getOutDegree();
	return os;
}

struct Arc
{
	//	Todo: keep track of persistence information when collapsing arcs
	using Node_ptr = Node*;

	Node_ptr source = nullptr;
	Node_ptr target = nullptr;
	size_t collapsedNodeCount = 0ul;
	float collapsedNodeSum = 0.0f;

	Arc(const Node_ptr& source, const Node_ptr& target)
		: source { source }, target { target }
	{
		assert(source != target);
		assert(source != nullptr);
		assert(target != nullptr);

		using namespace std;
		cout << "Adding arc " << this << " to out arcs of " << source << endl;
		source->outArcs.push_back(this);

		cout << "Adding arc " << this << " to in arcs of " << target << endl;
		target->inArcs.push_back(this);

		assert(source->outArcs.size() > 0);
		assert(target->inArcs.size() > 0);
	}
};

inline std::ostream& operator <<(std::ostream& os, const Arc& arc)
{
	os << "\"" << arc.source->position << "\"";
	os << " -> ";
	os << "\"" << arc.target->position << "\"";
	os << ";";
	os << "\t//  " << "node_count=" << arc.collapsedNodeCount << " ";
	os << "sample_sum=" << arc.collapsedNodeSum << " ";
	return os;
}

class MergeTree
{
public:
	using Node_ptr = Node*;
	using Arc_ptr = Arc*;
	using NodeList = std::vector<Node_ptr>;
	using ArcList = std::vector<Arc_ptr>;
	NodeList m_nodes;
	ArcList m_arcs;

	using TreeStructure = std::pair<NodeList, ArcList>;

	MergeTree();

	//	In time we'll use a unique_ptr for this
	using HeightSample_ptr = float const*;
	using Component_ptr = Component*;

	using ArcLookup = std::unordered_map<vec3, HeightSample_ptr>;
	using ComponentLookup = std::unordered_map<vec3, Component_ptr>;

	static constexpr auto MINUS_INFINITY = -std::numeric_limits<float>::infinity();
	static constexpr auto MINUS_INFINITY_PTR = &MINUS_INFINITY;

	static constexpr auto PLUS_INFINITY = std::numeric_limits<float>::infinity();
	static constexpr auto PLUS_INFINITY_PTR = &PLUS_INFINITY;

	size_t m_criticalNodes = 0;

	//	It may be possible to discard of the component lookup once the
	//	tree has been constructed and placed in the arc lookup...
	ArcLookup m_arcLookup;
	ComponentLookup m_componentLookup;

	enum class Neighbours
	{
		SIX,
		EIGHTEEN
	};
	std::deque<vec3> queueNeighbours(HeightField3D *heightfield, const vec3 &pos,
									 const Neighbours selectionStrategy) const;

	Component_ptr m_rootComponent = nullptr;

	bool removeRegularNodes();
	bool extractTree(HeightField3D *heightField, const bool reverse_arcs);

	std::string ToDotString(HeightField3D* heightField, bool reverse_arcs) const;

	Component_ptr findComponent(const vec3& pos3) const;

	int heightSort(const HeightSample_ptr& a, const HeightSample_ptr& b) const;
};

template <typename K, typename V>
inline std::ostream& operator<<(std::ostream& os, const std::unordered_map<K, V>& map)
{
	using namespace std;

	os << "Map: size = " << map.size() << endl;
	for (auto& o : map)
	{
		cout << "\t" << o.first << " : " << o.second << endl;
	}
	return os;
}

inline std::ostream& operator<<(std::ostream& os, const std::deque<vec3>& que)
{
	using namespace std;

	os << "Queue length: " << que.size() << endl;
	for (auto i = 0ul; i < que.size(); ++i)
	{
		os << "\t{ "
		   << que[i].x << ", "
		   << que[i].y << ", "
		   << que[i].z << " }"
		   << endl;
	}
	return os;
}

#endif // MERGE_TREE_H
