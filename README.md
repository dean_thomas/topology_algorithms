# README #

### What is this repository for? ###

* Computes the split and join tree of data defined as scalar samples in 3D space.

### How do I get set up? ###

* Some test data sets are provided in the source directory.
* Output graphs are in GraphVis (dot) format and shows the entire graph of connected components.
* Reduction of the graph to critical vertices only to follow...
